import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Modal,
  Picker,
  Dimensions,
  Linking,
} from 'react-native';
import { 
  Button, 
  Rating, 
  SearchBar,
  Icon,
} from 'react-native-elements';
const { width, height } = Dimensions.get('window')
import { DBConnect, GetObjectsList, GetSubCategories } from '../services/Objects';
import colors from '../helpers/colors';
import { LOCAL_ASSETS_BASE, URL_PUBLICATION_IMAGES } from '../config/urls';

export default class CategoryList extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      publications: [],
      category: null,
      subCategories: [],
      modalCategoryVisible: false,
      modalOrderVisible: false,
      currentCategory: null,
      currentOrder: null,
      currentOrderTemp: null,
      retrieved: [],
      firstLoad: true,
      isLoadingMore: false,
      orders: [
        {
          id: "destacados",
          name: "Destacados",
        },
        {
          id: "distancia",
          name: "Distancia",
        },
        {
          id: "valoracion",
          name: "Valoración",
        },
      ],
      searchText: '',
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentCategory !== this.state.currentCategory || prevState.currentOrder !== this.state.currentOrder) {
      this.loadData()
    }

    if (prevState.searchText !== this.state.searchText) {
      this.loadData()
    }
  }

  onOrderPress() {
    this.setState({
      modalOrderVisible: true,
      currentOrderTemp: this.state.currentOrder,
    })
  }

  orderChange = function(itemValue) {
    this.setState({
      currentOrderTemp: itemValue,
    })
  }

  doneOrderChange() {
    this.setState({
      modalOrderVisible: false,
      currentOrder: this.state.currentOrderTemp,
    })
  }

  componentDidMount() {
    this.setState({
      currentCategory: this.props.navigation.state.params.id,
      currentOrder: 'default',
      currentOrderTemp: 'default',
    })
  }

  loadData() {

    this.setState({
      isLoading: true,
    })

    DBConnect().then((db) => {

      GetSubCategories(db, this.state.currentCategory).then((response) => {
        if (this.state.firstLoad) {
          this.setState({
            subCategories: response,
            firstLoad: false,
          })
        }
      })
      console.log(this.state.currentOrder)
      GetObjectsList(db, this.state.currentCategory, this.state.currentOrder, [], this.props.screenProps.currentPosition, this.state.searchText).then((response) => {        
        this.setState({
          isLoading: false,
          publications: response,
          retrieved: response.map((p) => p.id),
        })
      })
    })
  }

  onSearchTextChange = function(newText) {
    this.setState({
      searchText: newText,
    })
  }

  isCloseToBottom = function({layoutMeasurement, contentOffset, contentSize}) {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  }

  loadMoreData() {
    if (!this.state.isLoadingMore) {
      this.setState({
        isLoadingMore: true,
      })

      DBConnect().then((db) => {
               
        GetObjectsList(db, this.state.currentCategory, this.state.currentOrder, this.state.retrieved, this.props.screenProps.currentPosition, this.state.searchText).then((response) => {        
          this.setState({
            publications: this.state.publications.concat(response),
            retrieved: this.state.retrieved.concat(response.map((p) => p.id)),
            isLoadingMore: false,
          })
        })
      })
    }
  }

  onLearnMore = (object) => {
    if (object.link == null) {
      this.props.navigation.navigate('PublicationDetail', { ...{id: object.id} });
    } else if (object.link.substring(0, 4) == "http") {
      Linking.openURL(object.link).catch(err => console.error('An error occurred', err))
    } else {
      this.props.navigation.navigate('PublicationDetail', { ...{id: object.link} });
    }
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.filterContainer}>
          <SearchBar
            lightTheme
            inputStyle={styles.searchFilter}
            containerStyle={styles.searchFilterContainer}
            round
            onChangeText={(text) => this.onSearchTextChange(text)}
            placeholder='Buscar...' />

          <Button
            buttonStyle={styles.orderFilter}
            containerViewStyle={styles.orderFilterContainer}
            icon={{name: 'menu', type: 'entypo', color: '#333'}}
            color='#333'
            onPress={() => this.onOrderPress()}
            title='Orden' />
        </View>

        {this.state.isLoading ? (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
        ) : (

        <ScrollView
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.loadMoreData();
            }
          }}
          scrollEventThrottle={400}
          >
          {this.state.publications.length == 0 && (
            <Text style={{textAlign: 'center', marginTop: 50,}}>No hay publicaciones disponibles</Text>
          ) }
          {this.state.publications.map((object) => (
          <TouchableOpacity style={styles.card} key={object.id} onPress={() => this.onLearnMore(object)}>
            <Image style={styles.image} source={{uri: LOCAL_ASSETS_BASE + URL_PUBLICATION_IMAGES + object.main_image}} resizeMode="cover" />
            <View style={styles.description}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text style={styles.title}>{object.name}</Text>
                </View>
                <View style={styles.seeMore}>
                  <Icon
                    name='zoom-in'
                    type='feather'
                    color='#4896EC'
                  />
                  <Text style={styles.seeMoreText}>Ver más</Text>
                </View>     
              </View>
              <View style={{flexDirection:'row'}}>
                {object.category_id.indexOf('nuestros-artistas') == -1 && (
                  <Rating
                  readonly
                  type="star"
                  fractions={1}
                  startingValue={object.votes_avg || 0}
                  imageSize={15}
                />
                )}
                {object.distance && (
                  <Text style={styles.distance}> | {Math.sqrt(object.distance).toFixed(2)} km</Text>
                )}
              </View>
            </View>
          </TouchableOpacity>
          ))}
          {this.state.isLoadingMore && <ActivityIndicator style={{marginBottom: 10}} />}
        </ScrollView>
        )}
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalCategoryVisible}
          onRequestClose={() => this.setState({modalCategoryVisible: false})}
          >
          <View style={{marginTop: 22}}>
            <Picker
              selectedValue={this.state.currentCategory}
              onValueChange={(itemValue, itemIndex) => this.categoryChange(itemValue)}
            >
              <Picker.Item key={this.state.currentCategory} label='Todos' value={this.state.currentCategory} />
            {this.state.subCategories.map((cat) => (
              <Picker.Item key={cat.id} label={cat.name} value={cat.id} />
            ))}
            </Picker>
            <Button 
              backgroundColor={colors.facebook} 
              onPress={() => this.setState({modalCategoryVisible: false})} 
              title='Aceptar' />
          </View>
        </Modal>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalOrderVisible}
          onRequestClose={() => this.setState({modalOrderVisible: false})}
          >
          <View style={{marginTop: 50}}>
            <Text style={styles.modalTitle}>Establecer Orden</Text>
            <Picker
              selectedValue={this.state.currentOrderTemp}
              onValueChange={(itemValue, itemIndex) => this.orderChange(itemValue)}
            >
            {this.state.orders.map((order) => (
              <Picker.Item key={order.id} label={order.name} value={order.id} />
            ))}
            </Picker>
            <Button 
              backgroundColor={colors.facebook}
              buttonStyle={{zIndex: 1000}}
              onPress={() => this.doneOrderChange()} 
              title='Aceptar' />
            <Button 
              buttonStyle={{marginTop: 15}}
              backgroundColor={colors.quora} 
              onPress={() => this.setState({modalOrderVisible: false})} 
              title='Cancelar' />
          </View>
        </Modal>
      </View>
    );
  }

}


const styles = {
  mainView: {
    backgroundColor: '#FFF',
    flex: 1,
  },

  card: {
    margin: 0,
  },

  image: {
    width: width,
    height: 250,
  },

  description: {
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 15,
  },

  distance: {
    color: '#666',
  },

  title: {
    marginBottom: 5,
    color: '#4896EC',
    fontSize: 22,
    fontWeight: 'bold',
  },

  seeMore: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    marginRight: 10,
  },

  seeMoreText: {
    marginTop: 5,
    color: '#4896EC',
    fontSize: 14,
    fontWeight: 'bold',
  },

  filterContainer: {
    height: 50,
    flexDirection: 'row',
  },

  orderFilterContainer: {
    flex: 1,
    marginLeft: 0,
    marginRight: 0,
    borderColor: '#BBB',
    borderWidth: 0.5,
  },

  orderFilter: {
    backgroundColor: '#EFEFEF',
    height: 50,
  },

  searchFilterContainer: {
    flex: 2,
  },

  searchFilter: {
    
  },

  modalTitle: {
    fontSize: 22,
    textAlign: 'center',
    fontWeight: '300',
  },

}
