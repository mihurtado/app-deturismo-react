import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, StyleSheet,
  Image, Dimensions, Modal, TouchableHighlight, TouchableOpacity, Linking, TextInput } from 'react-native';
import { List, ListItem, Text, Icon, Tabs, Tab, Rating, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window')
import ImageViewer from 'react-native-image-zoom-viewer';
import { DBConnect, GetSubCategories, GetCategoryPromotion } from '../services/Objects';
import { LOCAL_ASSETS_BASE, URL_PROMOTIONS_IMAGES } from '../config/urls';
import BackgroundImage from '../helpers/BackgroundImage';

export default class SubCategories extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC",
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoadingPromotions: true,
      isLoadingSubcategories: true,
      subCategories: [],
      promotion: null,
    }
    this.categoryId = this.props.navigation.state.params.id
    this.split_bg = null
    if (this.props.navigation.state.params.split_bg) {
      this.split_bg = require('../../assets/images/subcat_bg_spliter.png')
    }
  }

  componentDidMount() {
    DBConnect().then((db) => {
      GetSubCategories(db, this.categoryId).then((response) => {
        this.setState({
          subCategories: response,
          isLoadingSubcategories: false,
        })
      })

      GetCategoryPromotion(db, this.categoryId).then((response) => {
        if (response != null) {
          this.setState({
            promotion: response,
            isLoadingPromotions: false,
          })
        }
      })
    })
  }

  onPromotionPress = () => {
    if (this.state.promotion.url === null) {
      console.log('no url on promotion...')
    } else if (this.state.promotion.url.substring(0, 4) == "http") {
      Linking.openURL(this.state.promotion.url).catch(err => console.error('An error occurred', err))
    } else {
      this.props.navigation.navigate('PublicationDetail', { ...{id: this.state.promotion.url} });
    }
  }


  onSubcategoryPress = (object) => {
    if (object.link !== null) {
      if (object.link.substring(0, 4) == "http") {
        Linking.openURL(object.link).catch(err => console.error('An error occurred', err))
      } else {
        this.props.navigation.navigate(object.link)
      }
    } else {
      this.props.navigation.navigate('CategoryList', { ...object });
    }
  }

  render() {

    return (
      <View style={{flex: 1, backgroundColor: '#FFF'}}>

        <View style={{flex: 5}}>
          {this.state.isLoadingPromotions ? (
            <ActivityIndicator style={{marginTop: 50}} />
          ) : (
            <TouchableOpacity style={{flex: 1}} onPress={() => this.onPromotionPress()}> 
              <Image source={{uri: LOCAL_ASSETS_BASE + URL_PROMOTIONS_IMAGES + this.state.promotion.image}} style={styles.imageFullWidth} resizeMode='contain' />
            </TouchableOpacity>
          )}
        </View>

        <View style={{flex: 4, marginBottom: 20,}}>
          <BackgroundImage source={this.split_bg}>        
            <View style={styles.buttonsContainer}>

              {this.categoryId == "propiedades" && (
                <View style={{...styles.eachButtonContainer, width: width / 2}}>      
                  <Button
                    small
                    title="Arriendo"
                    buttonStyle={styles.subCategoryButtonHeader}
                    textStyle={styles.subCategoryButtonHeaderText}
                    />
                </View>
              )}
              {this.categoryId == "propiedades" && (
                <View style={{...styles.eachButtonContainer, width: width / 2}}>      
                  <Button
                    small
                    title="Venta"
                    buttonStyle={styles.subCategoryButtonHeader}
                    textStyle={styles.subCategoryButtonHeaderText}
                    />
                </View>
              )}

              {this.state.subCategories.map((category) => 
                {
                const styleWrapperWidth = {}
                const styleContainerWidth = {}
                if (category.width == 2) {
                  styleWrapperWidth.width = width
                  styleContainerWidth.width = width / 1.5
                } else {
                  styleWrapperWidth.width = width / 2
                  styleContainerWidth.width = width / 2
                }
                return (
                <View key={category.id} style={{...styles.eachButtonWrapper,...styleWrapperWidth}}>
                  <View style={{...styles.eachButtonContainer,...styleContainerWidth}}>      
                    <Button
                      small
                      title={category.name}
                      buttonStyle={styles.subCategoryButton}
                      textStyle={{textAlign: 'center'}}
                      onPress={() => this.onSubcategoryPress(category)} />
                    </View>
                  </View>
              )})}
            </View>
          </BackgroundImage>
        </View>
      </View>
    );
  }
}

const styles = {
  imageFullWidth: {
    width: width,
    flex: 1,
  },
  buttonsContainer: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: "space-around",
  },
  eachButtonWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  eachButtonContainer: {
    marginBottom: 10,
  },
  subCategoryButton: {
    backgroundColor: '#4896EC', 
    borderRadius: 10, 
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: .8,
    shadowRadius: 2,
    paddingVertical: 7,
  },
  subCategoryButtonHeader: {
    backgroundColor: '#1B3C66',
    borderRadius: 10, 
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: .8,
    shadowRadius: 2,
    paddingVertical: 7,
    marginBottom: 10,
  },
  subCategoryButtonHeaderText: {
    textAlign: 'center',
    color: '#ECCD67',
  },
}
