import React, { Component } from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
} from 'react-native-elements';
const { width, height } = Dimensions.get('window');
//import RNFetchBlob from 'react-native-fetch-blob';
import RNFS from 'react-native-fs';
import { 
  GetCurrentDatabaseVersion, 
  UpdateCurrentDatabaseVersion,
  RemoveDownloadRemaining,
  GetTotalDownloadsRemaining,
  SetDownloadsRemaining,
  GetDownloadsRemaining,
  ResetDownloadsRemainig,
  GetDownloadsDownloaded,
} from '../helpers/StoredData';
import { 
  URL_ASSETS_BASE, 
  URL_DATABASE_VERSION, 
  URL_DATABASE,
  LOCAL_DOCUMENTS_PATH,
  LOCAL_ASSETS_PATH,
} from '../config/urls';
import { 
  MAX_PARALL_DOWNLOADS, 
} from '../config/others';
import RNRestart from 'react-native-restart';
import BackgroundImage from '../helpers/BackgroundImage';
import { 
  DBClose, 
  GetAllPicturesUrl, 
  DBConnect,
} from '../services/Objects';
import {
  CheckConnection,
} from '../helpers/Connection';

export default class Updater extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isDownloadingImages: false,
      isDownloadingBD: false,
      totalDownloaded: 0,
      totalDownloads: 0,
      totalDownloadedFailed: 0,
      downloadedFailed: [],
      totalDownloadedPacket: 0,
      totalDownloadsSeted: 0,
    }
  }

  componentDidMount() {
    // Check version
    CheckConnection().then((isConnected) => {
      if (isConnected) {
        console.log('connected to internet')
        this.CheckUpdateDatabase()
      } else {
        console.log('NOT connected to internet')
        GetCurrentDatabaseVersion().then((currentVersion) => {
          GetTotalDownloadsRemaining().then((total) => {
            if (currentVersion === null) {
              Alert.alert(
                'No hay conexión a internet',
                'No hay conexión a internet y no tienes descargada la base de datos. Para poder utilizar la aplicación, conéctate para descargar los datos necesarios y reinicia la app.',
                [
                  {text: 'Ok', onPress: () => this.props.handleEndUpdate()},
                ],
                { cancelable: false }
              )
            } else if (total > 0) {
              Alert.alert(
                'No hay conexión a internet',
                'No hay conexión a internet y no tienes todos los datos descargados. Para una mejor experiencia, conéctate para descargar los datos restantes y reinicia la app.',
                [
                  {text: 'Ok', onPress: () => this.props.handleEndUpdate()},
                ],
                { cancelable: false }
              )
            }
          }) 
        })
      }
    })    
  }

  CheckUpdateDatabase = function(alertNoNewVersion) {
    GetCurrentDatabaseVersion().then((currentVersion) => {
      console.log('Current DB version ' + currentVersion)
      fetch(URL_ASSETS_BASE + URL_DATABASE_VERSION)
      .then((response) => response.text())
      .then((response) => {
        console.log('Server DB version ' + response)
        const version = parseFloat(response)
        if (currentVersion === null) {
          // No database, download
          this.UpdateDatabase(version)
        } else if (version > currentVersion)
        {
          Alert.alert(
            'Actualización de Datos',
            'Hay una nueva versión de la base de datos disponible. ¿Deseas actualizarla?',
            [
              {text: 'No aún', onPress: () => this.StartDownloadRemainingImages(), style: 'cancel'},
              {text: 'Actualizar!', onPress: () => this.UpdateDatabase(version)},
            ],
            { cancelable: false }
          )
        } else if (alertNoNewVersion === true) {
          Alert.alert(
            'Actualización de Datos',
            'Tienes la última versión!',
            [
              {text: 'Aceptar', onPress: () => this.props.handleEndUpdate()},
            ],
            { cancelable: false }
          )
        } else {
          // Check if all images donwloaded
          this.StartDownloadRemainingImages()
          // Just close updater
          //this.props.handleEndUpdate()
        }
      })
    }).catch((err) => {
      console.log('Error checking db update')
      this.props.handleEndUpdate()
    })
  }
  
  UpdateDatabase = function(version) {
    this.setState({
      isDownloadingBD: true,
    })
    console.log('Updating DB...')

    const download_options = {
      fromUrl: URL_ASSETS_BASE + URL_DATABASE,
      toFile: LOCAL_DOCUMENTS_PATH + '/database.sqlite',
    }

    RNFS.downloadFile(download_options).promise.then(res => {
      if (res.statusCode == 200) {
        UpdateCurrentDatabaseVersion(version).then(() => {
          this.setState({
            isDownloadingBD: false,
          })
          console.log('Restarting DB...')
          DBConnect().then((db) => {
            DBClose(db).then(() => {
              this.ResetDownloadImages()
            })
          })
        })
      } else {
        // Try again
        this.UpdateDatabase()
      }
    })
  }

  ResetDownloadImages() {

    GetDownloadsDownloaded().then((downloaded) => {
      DBConnect().then((db) => {
        GetAllPicturesUrl(db).then((urls) => {

          const final = {}

          for (let url in urls) {
            if (downloaded.indexOf(urls[url].id) < 0) {
              final[urls[url].id] = urls[url]
            }
          }

          SetDownloadsRemaining(final).then(() => {
            this.StartDownloadRemainingImages()
          })
        })
      })
    })

    /*
    // First delete all content
    RNFetchBlob.fs.unlink(LOCAL_ASSETS_PATH).then(() => {
      // Delete local storage vars
      ResetDownloadsRemainig().then(() => {
        DBConnect().then((db) => {
          GetAllPicturesUrl(db).then((urls) => {
            // Store donwload status empty
            SetDownloadsRemaining(urls).then(() => {
              this.StartDownloadRemainingImages()
            })
          })
        })
      })
    })
    */
  }

  StartDownloadRemainingImages() {

    console.log('started downloading images')

    GetDownloadsRemaining().then((downloads) => {
      const downloads_keys = Object.keys(downloads)
      console.log('total downloads ' + downloads_keys.length)
      if (downloads_keys.length > 0) {
        this.setState({
          downloads: downloads,
          downloadKeys: downloads_keys,
          isDownloadingImages: true,
          totalDownloads: downloads_keys.length,
          totalDownloaded: 0,
          totalDownloadedFailed: 0,
          downloadedFailed: [],
          totalDownloadedPacket: 0,
          totalDownloadsSeted: 0,
        }, () => {
          this.DownloadRemainingImages()
        })
      } else {
        this.checkAllDownloaded()
      }
    })
  }

  checkAllDownloaded(correct = true, elementid = null) {
    if (correct) {
      this.setState({
        totalDownloaded: this.state.totalDownloaded + 1,
        totalDownloadedPacket: this.state.totalDownloadedPacket + 1,
      }, () => {
        console.log(this.state.totalDownloaded + " " + this.state.totalDownloadedPacket + " " + this.state.totalDownloadedFailed + " " + this.state.totalDownloadsSeted)
        if (this.state.totalDownloadedPacket + this.state.totalDownloadedFailed >= this.state.totalDownloadsSeted) {
          if (this.state.totalDownloads - this.state.totalDownloaded + this.state.totalDownloadedFailed > 0) {
            this.DownloadRemainingImages()
          } else {
            this.props.handleEndUpdate()
          }
        }
      })
    } else {
      this.setState({
        totalDownloadedFailed: this.state.totalDownloadedFailed + 1,
        downloadedFailed: [...this.state.downloadedFailed, elementid],
      }, () => {
        console.log(this.state.totalDownloaded + " " + this.state.totalDownloadedPacket + " " + this.state.totalDownloadedFailed + " " + this.state.totalDownloadsSeted)
        if (this.state.totalDownloadedPacket + this.state.totalDownloadedFailed >= this.state.totalDownloadsSeted) {
          if (this.state.totalDownloads - this.state.totalDownloaded + this.state.totalDownloadedFailed > 0) {
            this.DownloadRemainingImages()
          } else {
            this.props.handleEndUpdate()
          }
        }
      })
    }
  }

  getTotalDownloadedPercentage() {
    if (this.state.totalDownloads > 0) {
      return Math.round(this.state.totalDownloaded / this.state.totalDownloads * 100)
    }
    return 0
  }

  DownloadFile = function(element) {
    const that = this
    // Create directory if it does not exists (if it does exists it will skip it)
    const directory_path = LOCAL_ASSETS_PATH + element.folder
    const download_options = {
      fromUrl: URL_ASSETS_BASE + element.folder + element.name,
      toFile: LOCAL_ASSETS_PATH + element.folder + element.name,
    }
    RNFS.mkdir(directory_path).then(() => {
      RNFS.downloadFile(download_options).promise.then(res => {
        if (res.statusCode == 200) {
          console.log('File downloaded at ' + LOCAL_ASSETS_PATH + element.folder + element.name)
          RemoveDownloadRemaining(element.id)
          that.checkAllDownloaded()
        } else {
          console.log('Download failed at ' + LOCAL_ASSETS_PATH + element.folder + element.name + '. Status code ' + res.statusCode)
          that.checkAllDownloaded(false, element.id)
        }
      }).catch(err => {
        console.log('Download failed at ' + LOCAL_ASSETS_PATH + element.folder + element.name + '. Err: ' + err)              
        that.checkAllDownloaded(false, element.id)
      })
    }).catch(err => {
      console.log('Creating dir failed, err ' + err)              
    })
  }

  DownloadRemainingImages() {
    const that = this
    // Put failed downloads into downloads
    let downloads = this.state.downloadedFailed
    const total_new_downloads = MAX_PARALL_DOWNLOADS - this.state.totalDownloadedFailed
    downloads = downloads.concat(this.state.downloadKeys.slice(-total_new_downloads))
    const toBeDownloded = Math.min(MAX_PARALL_DOWNLOADS, (this.state.downloadKeys.length + this.state.totalDownloadedFailed))
    let newDownloadKeys = null
    if (total_new_downloads > 0) {
      newDownloadKeys = this.state.downloadKeys.slice(0, -total_new_downloads)
    } else {
      newDownloadKeys = this.state.downloadKeys
    }
    this.setState({
      totalDownloadsSeted: toBeDownloded,
      totalDownloadedPacket: 0,
      totalDownloadedFailed: 0,
      downloadedFailed: [],
      downloadKeys: newDownloadKeys,
    }, () => {
      downloads.forEach((eid) => {
        // Closure for elementid
        (function(elementid) {
          const element = that.state.downloads[elementid]
          that.DownloadFile(element)
        }(eid))
      })
    })
  }

  render() {

    return (
      <BackgroundImage source={require('../../assets/images/bg.jpg')}>
        <Image source={require('../../assets/images/logo.png')} style={styles.logoImage} resizeMode={'contain'}  />
        <View style={styles.content}>
          <Text style={styles.text}>Cargando...</Text>
          <ActivityIndicator color='#000' style={styles.spiner} />
          {this.state.isDownloadingBD && (
            <Text style={styles.textSmall}>Descargando Base de Datos</Text>
          )}
          {this.state.isDownloadingImages && this.state.totalDownloads > 0 && (
            <Text style={styles.textSmall}>Descargando {this.getTotalDownloadedPercentage()}%</Text>
          )}
        </View>
      </BackgroundImage>
    )
  }

}

var styles = StyleSheet.create({

    logoImage: {
      marginTop: 10,
      marginBottom: 10,  
      width: width,
      height: width * 732 / 1800,
    },

    content: {
      backgroundColor: 'rgba(255,255,255,.7)',
      padding: 20,
      marginHorizontal: width / 5,
      marginVertical: height / 5,
    },

    spiner: {
      marginVertical: 10,
    },

    text: {
      fontSize: 16,
      textAlign: 'center',
    },

    textSmall: {
      fontSize: 12,
      textAlign: 'center',
    },
})