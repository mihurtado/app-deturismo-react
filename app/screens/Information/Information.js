import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class Information extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style = { styles.container }>
        <List>
          <ListItem
            onPress={() => this.props.navigation.navigate('AboutOurApp')}
            title={'Acerca De Nuestra Aplicación'}
            leftIcon={{ name: 'ios-information-circle-outline', type: 'ionicon', style: {color: 'blue'} }}
          />
          <ListItem
            onPress={() => this.props.navigation.navigate('PublishWithUs')}
            title={'Publica con nosotros'}
            leftIcon={{ name: 'ios-add-circle-outline', type: 'ionicon', style: {color: 'blue'} }}
          />
        </List>             
      </View>
    )
  }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})