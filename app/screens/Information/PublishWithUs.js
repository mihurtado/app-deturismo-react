import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
  FormLabel, 
  FormInput,
} from 'react-native-elements';
const { width, height } = Dimensions.get('window');
import colors from '../../helpers/colors';
import { 
  URL_PUBLISH_FORM,
} from '../../config/urls';

export default class PublishWithUs extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      comments: '',
      isLoading: false,
    }
  }

  onSend() {
    if (this.state.name.trim() == "" || this.state.email.trim() == "" || this.state.phone.trim() == "" || this.state.comments.trim() == "") {
      Alert.alert(
        'Completar campos',
        'Debes completar todos los campos del formulario!',
        [
          {text: 'Ok'},
        ],
        { cancelable: false }
      )
    } else {

      this.setState({
        isLoading: true,
      })

      fetch(URL_PUBLISH_FORM, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          comments: this.state.comments,
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.sent == true) {
          Alert.alert(
            'Mensaje enviado',
            'Tu mensaje ha sido enviado! Nos contactaremos contigo pronto. Gracias!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
          /*
          this.inputs.name.clearText()
          this.emailInput.clearText()
          this.phoneInput.clearText()
          this.commentsInput.clearText()
          */
        } else {
          Alert.alert(
            'Ha ocurrido un error. Inténtalo nuevamente.',
            responseJson.error,
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        }
        this.setState({
          isLoading: false,
        })
      })
      .catch((error) => {
        console.error(error);
        his.setState({
          isLoading: false,
        })
      });

    }
  }

  render() {

    return (
      <View style={styles.mainView}>

        {this.state.isLoading ? (
        <View style={{flex: 1, paddingTop: 50}}>
          <ActivityIndicator />
        </View>
        ) : (

          <ScrollView style = { styles.container }>
            <FormLabel>Nombre</FormLabel>
            <FormInput ref={input => this.nameInput = input} placeholder='Ingresa tu nombre' onChangeText={(text) => this.setState({name: text})}/>
            <FormLabel>Email</FormLabel>
            <FormInput ref={input => this.emailInput = input} placeholder='Ingresa tu email' onChangeText={(text) => this.setState({email: text})}/>
            <FormLabel>Teléfono</FormLabel>
            <FormInput ref={input => this.phoneInput = input} placeholder='Ingresa tu teléfono' onChangeText={(text) => this.setState({phone: text})}/>
            <FormLabel>Comentarios</FormLabel>
            <FormInput ref={input => this.commentsInput = input} placeholder='Ingresa tus comentarios' onChangeText={(text) => this.setState({comments: text})}/>
            <Button
              backgroundColor={colors.facebook} 
              onPress={() => this.onSend()} 
              title='Enviar'
              style={{marginTop: 20,}} />
          </ScrollView>
        )}
      </View>
    )
  }

}

var styles = StyleSheet.create({
  mainView: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 15, 
  },

  text: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'justify',
  },
})