import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class AboutOurApp extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <ScrollView style = { styles.container }>
        <Text style={styles.title}>DeTurismo Algarrobo</Text>
        <Text style={styles.text}>
          Somos la primera y más completa App de turismo de la zona. Nuestro objetivo es difundir información actualizada sobre alojamientos, lugares donde comer y/o beber, actividades, atractivos turísticos, monumentos, datos de interés y mucho más! 
        </Text>
        <Text style={styles.text}>
          Nosotros hacemos nuestro trabajo para que tu sólo te dediques a planear tu próximo viaje y disfrutar con los tuyos!
        </Text>
      </ScrollView>
    )
  }

}

var styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#FFF',
      padding: 15, 
  },

  text: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'justify',
  },

  title: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'justify',
    fontWeight: 'bold',
  },
})