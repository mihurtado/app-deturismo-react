import React, { Component } from 'react';
import { 
  View, 
  ScrollView, 
  ActivityIndicator, 
  StyleSheet,
  Image, 
  Dimensions, 
  Modal, 
  TouchableHighlight, 
  TouchableOpacity, 
  Linking, 
  TextInput, 
  Share, 
  Platform, 
  Alert,
  WebView,
} from 'react-native';
import { 
  List, 
  ListItem, 
  Text, 
  Icon, 
  Tabs, 
  Tab, 
  Rating, 
  Button,
  FormLabel, 
  FormInput,
} from 'react-native-elements';
const { width, height } = Dimensions.get('window')
import ImageViewer from 'react-native-image-zoom-viewer';
import colors from '../helpers/colors';
import { 
  DBConnect, 
  GetObject,
} from '../services/Objects';
import { 
  LOCAL_ASSETS_BASE, 
  URL_PUBLICATION_IMAGES, 
  URL_WEBPAGE,
  URL_CHAT_FRAME,
  URL_SEND_COMMENT,
  URL_VIEW_COMMENTS,
  URL_PUBLICATION_CONTACT_FORM,
} from '../config/urls';
import { 
  GetFacebookAccessToken,
  SetPublicationVisited,
} from '../helpers/StoredData';
import HTMLView from 'react-native-htmlview';
import {
  CheckConnection,
} from '../helpers/Connection';

export default class PublicationDetail extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC",
      display: 'none',
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoadingRating: true,
      object: [],
      modalPicturesVisible: false,
      modalRankVisible: false,
      modalCommentsVisible: false,
      selectedTab: 'description',
      isFavourite: false,
      isItinerary: false,
      isVisited: false,
      sendComment: '',
      sendRating: null,
      comments: [],
      form: {
        name: '',
        email: '',
        phone: '',
        comments: '',
      },
    }
  }

  changeTab (selectedTab) {
    this.setState({selectedTab: selectedTab})
  }

  setModalPicturesVisible(visible) {
    if (this.state.object.images.length > 0)
      this.setState({modalPicturesVisible: visible})
  }

  onFormSend() {
    console.log(this.state.form)
    if (this.state.form.name.trim() == "" || this.state.form.email.trim() == "" || this.state.form.phone.trim() == "" || this.state.form.comments.trim() == "") {
      Alert.alert(
        'Completar campos',
        'Debes completar todos los campos del formulario!',
        [
          {text: 'Ok'},
        ],
        { cancelable: false }
      )
    } else {

      this.setState({
        isLoading: true,
      })

      fetch(URL_PUBLICATION_CONTACT_FORM, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: this.state.form.name,
          email: this.state.form.email,
          phone: this.state.form.phone,
          comments: this.state.form.comments,
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.sent == true) {
          Alert.alert(
            'Mensaje enviado',
            'Tu mensaje ha sido enviado! Nos contactaremos contigo pronto. Gracias!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
          /*
          this.nameInput.clearText()
          this.emailInput.clearText()
          this.phoneInput.clearText()
          this.commentsInput.clearText()
          */
        } else {
          Alert.alert(
            'Ha ocurrido un error',
            responseJson.error,
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        }
        this.setState({
          isLoading: false,
        })
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          isLoading: false,
        })
      });

    }
  }

  componentDidMount() {

    DBConnect().then((db) => {
      GetObject(db, this.props.navigation.state.params.id, this.props.screenProps.currentPosition).then((response) => {
        this.setState({
          object: response,
          isLoading: false,
        })

        let itinerary = false
        if (response.id in this.props.screenProps.itineraries) {
          itinerary = true
        }
        let favorite = false
        if (response.id in this.props.screenProps.favorites) {
          favorite = true
        }
        this.setState({
          isItinerary: itinerary,
          isFavourite: favorite,
        }, () => {
          // Load comments
          CheckConnection().then((isConnected) => {
            if (isConnected) {
              fetch(URL_VIEW_COMMENTS, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  pub_id: this.state.object.id,
                })
              })
              .then((response) => response.json())
              .then((responseJson) => {
                if (responseJson.comments) {
                  this.setState({
                    object: { ...this.state.object, votes_avg: parseFloat(responseJson.rating)},
                    comments: responseJson.comments,
                    isLoadingRating: false,
                  })
                }
              })
              .catch((error) => {
                console.error(error);
              });
            } else {
              this.setState({
                isLoadingRating: false,
              })
            }
          })
        }) 
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    let itinerary = false
    if (nextProps.screenProps.itineraries.indexOf(this.state.object.id) >= 0) {
      itinerary = true
    }
  
    let favorite = false
    if (nextProps.screenProps.favorites.indexOf(this.state.object.id) >= 0) {
      favorite = true
    }
    this.setState({
      isFavourite: favorite,
      isItinerary: itinerary,
    })
  }

  onFavouritePress() {
    this.props.screenProps.updateFavorite(this.state.object.id, !this.state.isFavourite).then((res) => {
      if (res) {
        if (this.state.isFavourite) {
          Alert.alert(
            'Agregado!',
            'Agregado correctamente a tus favoritos!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        } else {
          Alert.alert(
            'Removido!',
            'Removido correctamente de tus favoritos!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        }
      } else {
        Alert.alert(
          'Iniciar Sesión',
          'Debes iniciar sesión para poder gestionar tus favoritos e itinerario. Dirígete a Mi Cuenta e inicia sesión con tu Facebook!',
          [
            {text: 'Iniciar Sesión', onPress: () => this.props.navigation.navigate('MyAccount')},
            {text: 'Cancel', style: 'cancel'},
          ],
          { cancelable: false }
        )
      }
    })
  }

  onItineraryPress() {
    this.props.screenProps.updateItinerary(this.state.object.id, !this.state.isItinerary).then((res) => {
      if (res) {
        if (this.state.isItinerary) {
          Alert.alert(
            'Agreado!',
            'Agregado correctamente a tu itinerario!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        } else {
          Alert.alert(
            'Removido!',
            'Removido correctamente de tu itinerario!',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        }
      } else {
        Alert.alert(
          'Iniciar Sesión',
          'Debes iniciar sesión para poder gestionar tus favoritos e itinerario. Dirígete a Mi Cuenta e inicia sesión con tu Facebook!',
          [
            {text: 'Iniciar Sesión', onPress: () => this.props.navigation.navigate('MyAccount')},
            {text: 'Cancel', style: 'cancel'},
          ],
          { cancelable: false }
        )
      }
    })
  }

  onVisitedPress() {
    if (!this.state.isVisited) {
      this.props.screenProps.checkFacebookLogin().then((res) => {
        if (res) {
          this.setState({
            modalRankVisible: true,
          })
        } else {
          Alert.alert(
            'Iniciar Sesión',
            'Debes iniciar sesión para poder ingresar un comentario y valoración. Dirígete a Mi Cuenta e inicia sesión con tu Facebook!',
            [
              {text: 'Iniciar Sesión', onPress: () => this.props.navigation.navigate('MyAccount')},
              {text: 'Cancel', style: 'cancel'},
            ],
            { cancelable: false }
          )
        }
      })
    } else {
      this.setState({
        modalCommentsVisible: true,
      })
    }
  }

  onShowCommentsPress() {
    this.setState({
      modalCommentsVisible: true,
    })
  }

  onChatPress() {
    this.setState({
      chatVisible: !this.state.chatVisible,
    })  
  }

  onHowToGetPress() {
    Platform.select({
        ios: () => {
            Linking.openURL('http://maps.apple.com/maps?daddr=' + this.state.object.latitude + ',' + this.state.object.longitude);
        },
        android: () => {
            Linking.openURL('http://maps.google.com/maps?daddr=' + this.state.object.latitude + ',' + this.state.object.longitude);
        }
    })();
  }

  onSharePress() {
    let description = this.state.object.description
    description = description.replace(/<\/?[^>]+(>|$)/g, '')
                             .replace(/&aacute;/g, 'á')
                             .replace(/&eacute;/g, 'é')
                             .replace(/&iacute;/g, 'í')
                             .replace(/&oacute;/g, 'ó')
                             .replace(/&uacute;/g, 'ú')
                             .replace(/&Aacute;/g, 'Á')
                             .replace(/&Eacute;/g, 'É')
                             .replace(/&Iacute;/g, 'Í')
                             .replace(/&Oacute;/g, 'Ó')
                             .replace(/&Uacute;/g, 'Ú')
                             .replace(/&ntilde;/g, 'ñ')
                             .replace(/&Ntilde;/g, 'Ñ')

    Share.share({
      title: 'Deturismo Algarrobo',
      url: URL_WEBPAGE,
      message: this.state.object.name + ' - ' + description,
    }, {
      dialogTitle: 'Compartir ' + this.state.object.name,
    })
  }

  onRankSend() {

    GetFacebookAccessToken().then((token) => {
      if (token === null) {
        Alert.alert(
          'Iniciar Sesión',
          'Debes iniciar sesión para poder comentar. Dirígete a Mi Cuenta e inicia sesión con tu Facebook!',
          [
            {text: 'Iniciar Sesión', onPress: () => this.props.navigation.navigate('MyAccount')},
            {text: 'Cancel', style: 'cancel'},
          ],
          { cancelable: false }
        )
      } else {
        if (this.state.sendRating === null) {
          Alert.alert(
            'Completar campos',
            'Debes completar tu valoración y, si quieres, agregar un comentario.',
            [
              {text: 'Ok'},
            ],
            { cancelable: false }
          )
        } else {

          this.setState({
            isLoading: true,
          })
          fetch(URL_SEND_COMMENT, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              pub_id: this.state.object.id,
              fb_id: token,
              rating: this.state.sendRating,
              comments: this.state.sendComment,
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.sent == true) {
              Alert.alert(
                'Valoración enviada',
                'Tu valoración ha sido registrada. Gracias!',
                [
                  {text: 'Ok'},
                ],
                { cancelable: false }
              )
              /*
              this.comments.clearText()
              */
              SetPublicationVisited(this.state.object.id, true).then(() => {
                this.setState({
                  isVisited: true,
                  modalRankVisible: false,
                })
              })

            } else {
              Alert.alert(
                'Ha ocurrido un error',
                responseJson.errors,
                [
                  {text: 'Ok'},
                ],
                { cancelable: false }
              )
            }
            this.setState({
              isLoading: false,
            })
          })
          .catch((error) => {
            console.error(error);
            this.setState({
              isLoading: false,
            })
          });
        }
      }
    })
  }

  onLocationOpen = (object) => {
    this.props.navigation.navigate('PublicationMap', { ...object });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    let images = []
    this.state.object.images.forEach(function(image) {
      images.push({url: LOCAL_ASSETS_BASE + URL_PUBLICATION_IMAGES + image.url})
    }, this);
    return (
      <View style={{flex: 1, backgroundColor: '#FFF'}}>
        <View style={{flex: 2, backgroundColor: '#FFF'}}>
          <TouchableOpacity
            style={styles.back}
            onPress={() => this.props.navigation.goBack()}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>&larr;</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.share}
            onPress={() => this.onSharePress()}
          >
            <Icon name='share-apple' type='evilicon' />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.multiplePictures}
            onPress={() => this.setModalPicturesVisible(true)}
          >
            <Icon name='image-multiple' type='material-community' />
          </TouchableOpacity>

          <Modal 
            animationType={"slide"}
            visible={this.state.modalPicturesVisible}
            transparent={true}
            onRequestClose={() => this.setState({modalPicturesVisible: false})}>
              <Icon
                name='close'
                type='evillcons'
                color='#FFF'
                onPress={() => {
                  this.setModalPicturesVisible(false)
                }}
                size = {40}
                style = {{position: 'absolute', top: 10, right: 10, zIndex: 9999}}
              />
              <ImageViewer imageUrls={images}/>
          </Modal>

          <TouchableHighlight onPress={() => {this.setModalPicturesVisible(true)}} style={{flex: 1}}>
            <Image source={{uri: LOCAL_ASSETS_BASE + URL_PUBLICATION_IMAGES + this.state.object.main_image}} style={styles.imageFullWidth} resizeMode='cover' />
          </TouchableHighlight>
        </View>

        <View style={{flex: 3}}>
          <View style={styles.tabContainer}>
            <TouchableHighlight 
              style={styles.iconTouchContainer} 
              onPress={() => {this.onFavouritePress()}}
              >
              <View style={styles.iconContainer}>
                <Icon
                  reverse={this.state.isFavourite}
                  reverseColor='#75C0E6'
                  name='favorite-border'
                  type='material-icons'
                  style={styles.tabIcon}
                  color='#FFF'
                  size={24}
                />
                <Text style={styles.iconText}>Favoritos</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight 
            style={styles.iconTouchContainer} 
            onPress={() => {this.onItineraryPress()}}
            >
              <View style={styles.iconContainer}>
                <Icon
                  reverse={this.state.isItinerary}
                  reverseColor='#75C0E6'
                  name='routes'
                  type='material-community'
                  style={styles.tabIcon}
                  color='#FFF'
                  size={24}
                />
                <Text style={styles.iconText}>Itinerario</Text>
              </View>
            </TouchableHighlight>
            {this.state.object.latitude && this.state.object.longitude && (
            <TouchableHighlight 
            style={styles.iconTouchContainer} 
            onPress={() => {this.onLocationOpen(this.state.object)}}
            >
              <View style={styles.iconContainer}>
                <Icon
                  name='map-marker'
                  type='font-awesome'
                  style={styles.tabIcon}
                  color='#FFF'
                  size={24}
                />
                <Text style={styles.iconText}>Mapa</Text>
              </View>
            </TouchableHighlight>
            )}
            {this.state.object.latitude && this.state.object.longitude && (
            <TouchableHighlight 
            style={styles.iconTouchContainer} 
            onPress={() => {this.onHowToGetPress()}}
            >
              <View style={styles.iconContainer}>
                <Icon
                  name='directions-fork'
                  type='material-community'
                  style={styles.tabIcon}
                  color='#FFF'
                  size={24}
                />
                <Text style={styles.iconText}>Cómo Llegar</Text>
              </View>
            </TouchableHighlight>
            )}
            <TouchableHighlight 
            style={styles.iconTouchContainer} 
            onPress={() => {this.onVisitedPress()}}
            >
              <View style={styles.iconContainer}>
                <Icon
                  reverse={this.state.isVisited}
                  reverseColor='#75C0E6'
                  name='check'
                  type='entypo'
                  style={styles.tabIcon}
                  color='#FFF'
                  size={24}
                />
                <Text style={styles.iconText}>Visitado</Text>
              </View>
            </TouchableHighlight>
            {this.state.object.category_id.indexOf('atractivos') == -1 && (
              <TouchableHighlight 
              style={styles.iconTouchContainer} 
              onPress={() => {this.onChatPress()}}
              >
                <View style={styles.iconContainer}>
                  <Icon
                    reverse={this.state.chatVisible}
                    reverseColor='#75C0E6'
                    name='ios-chatbubbles-outline'
                    type='ionicon'
                    style={styles.tabIcon}
                    color='#FFF'
                    size={24}
                  />
                  <Text style={styles.iconText}>Consultar</Text>
                </View>
              </TouchableHighlight>
            )}            
          </View>

          {this.state.chatVisible ? (
            <View style={{flex: 1}}>
              <TouchableOpacity
              style={styles.closeChat}
              onPress={() => {this.onChatPress()}}
              >
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#FFF' }}>X</Text>
              </TouchableOpacity>
              {/*
              <WebView
              source={{uri: URL_CHAT_FRAME}}
              />
              */}
              <ScrollView style = { styles.formContainer }>
                <FormLabel>Nombre</FormLabel>
                <FormInput ref={input => this.nameInput = input} placeholder='Ingresa tu nombre' onChangeText={(text) => this.setState(prevState => ({form: {...prevState.form, name: text}}))}/>
                <FormLabel>Email</FormLabel>
                <FormInput ref={input => this.emailInput = input} placeholder='Ingresa tu email' onChangeText={(text) => this.setState(prevState => ({form: {...prevState.form, email: text}}))}/>
                <FormLabel>Teléfono</FormLabel>
                <FormInput ref={input => this.phoneInput = input} placeholder='Ingresa tu teléfono' onChangeText={(text) => this.setState(prevState => ({form: {...prevState.form, phone: text}}))}/>
                <FormLabel>Comentarios</FormLabel>
                <FormInput ref={input => this.commentsInput = input} placeholder='Ingresa tus comentarios' onChangeText={(text) => this.setState(prevState => ({form: {...prevState.form, comments: text}}))}/>
                <Button 
                  backgroundColor={colors.facebook} 
                  onPress={() => this.onFormSend()} 
                  title='Enviar'
                  style={{marginTop: 20,}} />
              </ScrollView>
            </View>
          ) : (
            <ScrollView style={styles.descriptionContainer}>
              <Text style={styles.title}>{this.state.object.name}</Text>
              <View style={{flexDirection:'row'}}>
                {this.state.object.category_id.indexOf('nuestros-artistas') == -1 && this.state.isLoadingRating && (
                  <View style={{flex: 1}}>
                    <ActivityIndicator />
                  </View>
                )}
                {this.state.object.category_id.indexOf('nuestros-artistas') == -1 && !this.state.isLoadingRating && (
                  <TouchableOpacity onPress={() => this.onVisitedPress()}>
                    <Rating
                      readonly
                      type="star"
                      fractions={1}
                      startingValue={this.state.object.votes_avg || 0}
                      imageSize={15}
                    />
                  </TouchableOpacity>
                )}
                {this.state.object.distance && (
                <Text style={styles.distance}> | {Math.sqrt(this.state.object.distance).toFixed(2)} km</Text>
                )}
                {this.state.object.category_id.indexOf('nuestros-artistas') == -1 && (
                  <TouchableOpacity onPress={() => this.onShowCommentsPress()}>
                    <Text style={styles.distance}> | Ver comentarios</Text>
                  </TouchableOpacity>
                )}
              </View>
              <HTMLView addLineBreaks={true} stylesheet={descriptionStyles} value={'<div>' + this.state.object.description + '</div>'} />

              {this.state.object.equipment !== null && this.state.object.equipment != "" &&
              <Text style={styles.equipment}>
                <Text style={{fontWeight: 'bold'}}>Equipamiento: </Text>
                {this.state.object.equipment}
              </Text>
              }

              {this.state.object.address !== null && this.state.object.address != "" &&
              <Text style={styles.location}>
                <Text style={{fontWeight: 'bold'}}>Ubicación: </Text>
                {this.state.object.address}
              </Text>
              }
              
              <View style={{flexDirection: 'row', flex: 1, marginTop: 5, justifyContent: 'space-between'}}>
                {this.state.object.email !== null && this.state.object.email != "" &&
                <View style={{flex: 1}}>
                  <Button
                    small
                    buttonStyle={styles.actionButton}
                    textStyle={styles.actionButtonText}
                    icon={{name: 'email', type: 'entypo', style: styles.actionButtonIcon}}
                    title='Email'
                    onPress={() => (Linking.openURL('mailto:' + this.state.object.email).catch(err => console.error('An error occurred', err))) } />
                  </View>
                }
                
                {this.state.object.phone !== null && this.state.object.phone != "" &&
                <View style={{flex: 1}}>
                  <Button
                    small
                    buttonStyle={styles.actionButton}
                    textStyle={styles.actionButtonText}
                    icon={{name: 'phone', type: 'entypo', style: styles.actionButtonIcon}}
                    title='Fono'
                    onPress={() => (Linking.openURL('tel:' + this.state.object.phone).catch(err => console.error('An error occurred', err))) } />
                  </View>
                }

                {this.state.object.cellphone !== null && this.state.object.cellphone != "" &&
                <View style={{flex: 1}}>
                  <Button
                    small
                    buttonStyle={styles.actionButton}
                    textStyle={styles.actionButtonText}
                    icon={{name: 'phone', type: 'entypo', style: styles.actionButtonIcon}}
                    title='Fono'
                    onPress={() => (Linking.openURL('tel:' + this.state.object.cellphone).catch(err => console.error('An error occurred', err))) } />
                  </View>
                }

                {this.state.object.url !== null && this.state.object.url != "" &&
                <View style={{flex: 1}}>
                  <Button
                    small
                    buttonStyle={styles.actionButton}
                    textStyle={styles.actionButtonText}
                    icon={{name: 'web', type: 'material-community', style: styles.actionButtonIcon}}
                    title='Web'
                    onPress={() => (Linking.openURL(this.state.object.url).catch(err => console.error('An error occurred', err))) } />
                  </View>
                }
              </View>
            </ScrollView>
          )}

        </View>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalRankVisible}
          onRequestClose={() => this.setState({modalRankVisible: false})}
          >
          <View style={{flex: 1, marginTop: 22, padding: 20}}>
            <Text h4>Valora {this.state.object.name}</Text>
            <Rating
              type="star"
              fractions={1}
              startingValue={0}
              imageSize={40}
              style={{ paddingVertical: 10, justifyContent: 'center', alignItems: 'center',}}
              onFinishRating={(rat) => this.setState({sendRating: rat})}
            />
            <TextInput
                style={{height: 120,}}
                multiline = {true}
                numberOfLines = {4}
                placeholder="Ingresa aquí tu comentario..."
                onChangeText={(text) => this.setState({sendComment: text})}
              />
            <Button 
              style={{marginTop: 20,}}
              backgroundColor={colors.facebook} 
              onPress={() => this.onRankSend()} 
              title='Aceptar' />
            <Button 
              style={{marginTop: 10,}}
              backgroundColor={colors.quora} 
              onPress={() => this.setState({modalRankVisible: false})} 
              title='Cancelar' />
          </View>
        </Modal>

        <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.state.modalCommentsVisible}
        onRequestClose={() => this.setState({modalCommentsVisible: false})}
        >
        <View style={{marginTop: 22, padding: 20}}>
          <TouchableOpacity
              style={styles.closeComments}
              onPress={() => {this.setState({modalCommentsVisible: false,})}}
              >
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#FFF' }}>X</Text>
            </TouchableOpacity>
          <Text h4>Comentarios {this.state.object.name}</Text>
          <ScrollView>
            {this.state.comments.length == 0 && (
              <Text style={{textAlign: 'center', marginTop: 50,}}>No hay comentarios disponibles</Text>
            ) }
            {this.state.comments.map((comment) => (
            <View style={styles.eachComment} key={comment.id}>
              <Rating
                readonly
                type="star"
                fractions={1}
                startingValue={parseFloat(comment.rating) || 0}
                imageSize={15}
              />
              <Text>{comment.comment}</Text>
            </View>
            ))}

          </ScrollView>
        </View>
      </Modal>
        
      </View>
    );
  }
}

const descriptionStyles = {
  div: {
    textAlign: 'justify',
    fontWeight: '200',
    color: '#444',
    fontSize: 15,
    marginBottom: 10,
    marginTop: 10,
  },
}

const styles = {
  imageFullWidth: {
    width: width,
    flex: 1,
  },

  back: {
    position: 'absolute',
    top: 20,
    left: 6,
    backgroundColor: 'rgba(255,255,255,0.4)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  share: {
    position: 'absolute',
    top: 20,
    right: 6,
    backgroundColor: 'rgba(255,255,255,0.4)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  multiplePictures: {
    position: 'absolute',
    bottom: 5,
    right: 6,
    backgroundColor: 'rgba(255,255,255,0.4)',
    padding: 4,
    borderRadius: 15,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  actionButton: {
    flex: 1,
    marginBottom: 15,
    backgroundColor: '#4896EC', 
    borderRadius: 7,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: .8,
    shadowRadius: 2,
    paddingVertical: 7,
    paddingHorizontal: 4,
  },
  actionButtonText: {
    fontSize: 11,
    marginLeft: 0,
    paddingLeft: 0,
  },
  actionButtonIcon: {
    marginRight: 3,
  },
  tabContainer: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: '#000C1C',
  },

  iconTouchContainer: {
    flexDirection: 'column', 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
  },

  iconContainer: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
  },

  iconText: {
    fontSize: 10,
    color: '#FFF',
  },

  tabIcon: {
    height: 30,
  },

  container: {
    backgroundColor: '#FFFFFF',
  },

  descriptionContainer: {
    padding: 10,
    flex: 1,
  },

  location: {
    textAlign: 'justify',
    fontWeight: '200',
    color: '#444',
    fontSize: 14,
    marginBottom: 10,
  },

  equipment: {
    textAlign: 'justify',
    fontWeight: '200',
    color: '#444',
    fontSize: 14,
    marginBottom: 10,
  },

  distance: {
    color: '#444',
  },

  title: {
    color: '#4896EC',
    fontSize: 22,
    marginBottom: 5,
    fontWeight: 'bold',
  },

  closeChat: {
    position: 'absolute',
    top: 5,
    right: 5,
    backgroundColor: 'rgba(0,0,0,0.9)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  closeComments: {
    position: 'absolute',
    top: 5,
    right: 5,
    backgroundColor: 'rgba(0,0,0,0.9)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  formContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 5, 
  },

  eachComment: {
    marginTop: 5,
    marginBottom: 5,
  },
}
