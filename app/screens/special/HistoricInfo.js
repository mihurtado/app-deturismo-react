import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class HistoricInfo extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <Image source={require('../../../assets/images/resena-historica.jpg')} style={styles.image} resizeMode={'contain'}  />
        <ScrollView style={styles.textContainer}>
          <Text style={styles.text}>Algarrobo es una comuna Balneario del Litoral Central de Chile, ubicada en la Provincia de San Antonio, V región, a 107 km. de la Ciudad de Santiago. Es conocida como la Capital náutica de Chile.
          </Text>
          
          <Text style={styles.text}>Su nombre se debe a la presencia de un árbol nativo homónimo (Prosopis chilenos). Su Territorio comprende 176 km2 , limita al Norte y al  Este con Casablanca, al Sur con la comuna de El Quisco y al Oeste con el Océano Pacífico.
          </Text>

          <Text style={styles.text}>Nuestra Comuna se divide en varios sectores, en donde se ubican la mayoría de la Población. Estos son: El Canelo, Albatros, El Litre, Las Brisas Algarrobas, Aguas Marinas, San José del Peumal, San Gerónimo, Mirasol, El Yeco, San José, Tunquen.
          </Text>

          <Text style={styles.text}>La Población Total de Algarrobo, es de 10.588 Habitantes, 5.374 hombres y 5.214 mujeres.
          </Text>

          <Text style={styles.text}>Se caracteriza por ser una comuna atractiva y por encontrar diversos espacios aptos para la práctica de varios deportes náuticos, terrestres y aéreos. Posee además, una gran variedad  y riqueza gastronómica, ligada principalmente a los frutos del mar.
          </Text>
        </ScrollView>
      </View>
    )
  }

}

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFF',
  },

  textContainer: {
    flex: 1,
    padding: 15, 
  },

  image: {
    marginBottom: 10,
    width: width,
    height: width * 220 / 500,
  },

  text: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'justify',
  },
})