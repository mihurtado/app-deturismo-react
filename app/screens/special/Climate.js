import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
} from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class Climate extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <Image source={require('../../../assets/images/clima.jpg')} style={styles.image} resizeMode={'contain'}  />
        <ScrollView style={styles.textContainer}>
          <Text style={styles.text}>El Litoral Central de Chile posee un clima único y privilegiado, 
          llamado Clima Templado Cálido Occidental, Dicho clima está determinado por 
          corrientes marinas frías que barren las costas y los vientos del mar que moderan 
          las temperaturas. Como consecuencia, poseen poca diferencia de temperatura 
          durante el año y en general son zonas de moderadas lluvias de invierno y 
          sequía en verano.</Text>
          
          <Text style={styles.text}>La temperatura do la corriente de Humboldt determina el clima que, en 
          Valparaíso, alcanza a una temperatura media en verano de 20 º C a las 13 hrs 
          y en invierno 15 º C. La diferencia media entre las temperaturas del día y la 
          noche en invierno y verano es de 5ºC. La humedad media relativa del aire a 
          mediodía en verano es de 65%. La precipitación anual tiene una media de 462 mm 
          de lluvia, concentrada en los meses de mayo a octubre; los meses de noviembre 
          a abril son considerados secos.</Text>
        </ScrollView>
      </View>
    )
  }

}

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFF',
  },

  textContainer: {
    flex: 1,
    padding: 15, 
  },

  image: {
    marginBottom: 10,
    width: width,
    height: width * 220 / 500,
  },

  text: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'justify',
  },
})