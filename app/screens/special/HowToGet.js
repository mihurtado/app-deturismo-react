import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class HowToGet extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <Image source={require('../../../assets/images/como-llegar.jpg')} style={styles.image} resizeMode={'contain'}  />
        <ScrollView style={styles.textContainer}>
          <Text style={styles.title}>Desde Santiago:</Text>
          <Text style={styles.text}>
            Una buena alternativa es tomar la ruta 68 en Dirección Valparaíso y llegar hasta el cruce con Casablanca (70 km. aprox.) ahí se dobla a la izquierda (Restaurant Bariloche) en dirección a la costa por la ruta F 90 (aproximadamente 35 km. hacia el Oeste).
          </Text>
          <Text style={styles.title}>Por Autopista del Sol:</Text>
          <Text style={styles.text}>
            Otra alternativa es partir desde el sur de Santiago, por la Autopista El Sol (ruta 78) y luego seguir por la ruta costera que pasa por todo el litoral. Si eliges esta ruta debes doblar a la derecha en el desvío que te lleva hacia San Antonio y pasar por los balnearios de San Sebastián, Las Cruces, El Tabo, Isla Negra, Punta de Tralca y El Quisco antes de llegar a Algarrobo.
          </Text>
          <Text style={styles.title}>Desde Valparaíso y Viña del Mar:</Text>
          <Text style={styles.text}>
          También se puede llegar por la ruta 68 (50 km. aprox.) y luego tomar la ruta F 90 a la altura del cruce de Casablanca a mano derecha. Desde Valparaíso y Viña se puede llegar vía Pullman.
          </Text>
        </ScrollView>
      </View>
    )
  }

}

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFF',
  },

  textContainer: {
    flex: 1,
    padding: 15, 
  },

  image: {
    marginBottom: 10,
    width: width,
    height: width * 220 / 500,
  },

  text: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'justify',
  },

  title: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'justify',
    fontWeight: 'bold',
  },
})