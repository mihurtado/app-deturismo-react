import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
  Icon,
} from 'react-native-elements';
import Sound from 'react-native-sound';
import { 
  DBConnect, 
  GetNaturalSounds,
} from '../../services/Objects';
import {
  URL_ASSETS_BASE,  
  LOCAL_ASSETS_BASE, 
  URL_NATURAL_SOUNDS_IMAGES,
  URL_NATURAL_SOUNDS_SOUNDS,
} from '../../config/urls';


const { width, height } = Dimensions.get('window');

export default class NaturalSounds extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      sounds: [],
      playingSound: null,
    }
  }

  loadData() {

    this.setState({
      isLoading: true,
    })

    DBConnect().then((db) => {

      const resetStart = 0
      
      GetNaturalSounds(db).then((response) => {        
        this.setState({
          isLoading: false,
          sounds: response,
          playingSoundId: null,
          playSound: null,
          loadingSoundId: null,
        })
      })
    })
  }

  componentDidMount() {
    this.loadData()
  }

  playSound(object) {
  
    const callback = (error, sound) => {
      if (error) {
        Alert.alert('error', error.message);
        return;
      }
      console.log('Playing sound')
      this.setState({
        loadingSoundId: null,
        playingSound: sound,
        playingSoundId: object.id,
      })
      sound.play(() => {
        // Success counts as getting to the end
        // Release when it's done so we're not using up resources
        sound.release();
        this.setState({
          playingSoundId: null,
          playingSound: null,
        })
      });
    };

    // Stop current sound
    if (this.state.playingSoundId !== null) {
      this.state.playingSound.stop()
      this.state.playingSound.release()
    }
    
    const sound = new Sound(URL_ASSETS_BASE + URL_NATURAL_SOUNDS_SOUNDS + object.sound, '', error => callback(error, sound));
    this.setState({
      playingSoundId: null,
      playingSound: null,
      loadingSoundId: object.id,
    })
  }

  stopSound() {
    // Stop current sound
    if (this.state.playingSoundId !== null) {
      this.state.playingSound.stop()
      this.state.playingSound.release()
    }
    this.setState({
      playingSound: null,
      playingSoundId: null,
    })
  }

  render() {

    return (
      <View style={styles.mainView}>
      
        {this.state.isLoading ? (
        <View style={{flex: 1, paddingTop: 50}}>
          <ActivityIndicator />
        </View>
        ) : (
        <ScrollView
          >
          {this.state.sounds.length == 0 && (
            <Text style={{textAlign: 'center', marginTop: 50,}}>No hay sonidos disponibles</Text>
          ) }
          {this.state.sounds.map((object) => (
          <View style={styles.objectContainer} key={object.id}>
            <View style={styles.playAndImageContainer}>
              <View style={styles.playButtonWrapper}>
              {this.state.playingSoundId == object.id && (
                <TouchableOpacity style={styles.playButtonTouch} onPress={() => this.stopSound(object)}>
                  <Icon name='pause-circle-outline' type='material-community' color='#FFF' size={80} style={styles.playButtonIcon} />
                  <Text style={styles.playButtonText}>Pausa</Text>
                </TouchableOpacity>
              )}
              {this.state.loadingSoundId == object.id && (
                <TouchableOpacity style={styles.playButtonTouch} onPress={() => this.playSound(object)}>
                  <ActivityIndicator color='#FFF' size='large'  />
                  <Text style={styles.playButtonText}>Cargando...</Text>
                </TouchableOpacity>
              )}
              {this.state.loadingSoundId != object.id && this.state.playingSoundId != object.id && (
                <TouchableOpacity style={styles.playButtonTouch} onPress={() => this.playSound(object)}>
                  <Icon name='play-circle-outline' type='material-community' color='#FFF' size={80} style={styles.playButtonIcon} />
                  <Text style={styles.playButtonText}>Reproducir</Text>
                </TouchableOpacity>
              )}
              </View>
              <View style={styles.imageContainer}>
                <Image style={styles.image} source={{uri: LOCAL_ASSETS_BASE + URL_NATURAL_SOUNDS_IMAGES + object.picture}} resizeMode="cover" />              
              </View>
            </View>
            <View style={styles.descriptionWrapper}>
              <Text style={styles.title}>{object.name}</Text>
              <Text style={styles.description}>{object.description}</Text>
            </View>
          </View>
          ))}
        </ScrollView>      
        )}
      </View>
    )
  }

}

var styles = StyleSheet.create({

  mainView: {
    backgroundColor: '#FFF',
    flex: 1,
  },

  objectContainer: {
    margin: 0,
    marginBottom: 15,
  },

  playAndImageContainer: {
    flexDirection: 'row',
    flex: 1,
  },

  playButtonWrapper: {
    flex: 1,
    backgroundColor: '#FFCC29',
    justifyContent: 'center',
    flexDirection: 'column',
  },

  playButtonTouch: {
    
  },

  playButtonIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  playButtonText: {
    flex: 1,
    textAlign: 'center',
    color: '#FFF',
    fontSize: 14,
  },

  playButton: {
    borderRadius: 100,
    height: 50,
    backgroundColor: 'transparent',
    padding: 0,
  },
  
  imageContainer: {
    flex: 2,
  },

  image: {
    width: width * 2 / 3,
    height: 250,
  },

  descriptionWrapper: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 15,
  },

  description: {
    textAlign: 'justify',
  },

  title: {
    marginBottom: 5,
    color: '#000',
    fontSize: 22,
    fontWeight: 'bold',
  },

})