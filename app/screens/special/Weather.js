import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

import Common from './weather/Common';
import WeekView from './weather/WeekView';

const flex = [2,1,6,6]

export default class Weather extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    var flexItemHeight = height / flex.reduce(function(a, b){return a+b;});
    this.state = {
      isLoading: true,
      current: {
          city: "",
          weather: "",
          temp: "",
          date: "",
          icon: ""
      },
      weekViewData: [],
    }
  }

  getCurrent = function(data){
    var now = Date.now();
    var current = data.list.filter(function(item) {
        return Math.abs(item.dt * 1000 - now) < (3 * 60 * 60 * 1000); // less than 3 hrs
    })[0];
    var _today = Common.today(now);
    return {
        city: data.city.name + ',' + data.city.country,
        weather: current.weather[0].description,
        temp: Common.f2c(current.main.temp),
        date: _today[1] + '/' + _today[2] + '/' + _today[0],
        icon: Common.icon(current.weather[0].icon)
    }
  }

  componentDidMount() {
    const that = this;
    const url = 'https://api.openweathermap.org/data/2.5/forecast?q=valparaiso,cl&lang=es_cl&appid=37f4d2e14c7c6ebe4eb37ff9bfa4e7fc'
    
    fetch(url)
      .then((response) => response.json())
      .then(function(data) {
        var current = that.getCurrent(data);
        that.setState({
            current: current,
            weekViewData: data.list,
            isLoading: false,
        })
          
      })
      .catch((error) => {
        console.log(error)
      })
      .done();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (

      <View style = { styles.container }>
        <View style = { styles.city_day }>
          <Text style = { styles.city_day_text }> 
            Algarrobo - { this.state.current.date }
          </Text>
        </View>
        <Text style = { styles.status }>
          { this.state.current.weather } { this.state.current.temp }°C
        </Text> 
        <Text style = { [styles.current, styles.weather_icon] }> 
          { this.state.current.icon } 
        </Text>
        <WeekView data={this.state.weekViewData} flex={flex.slice(3)}/>

      </View>
    )
  }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3d566e',
        flexDirection: 'column'
    },
    city_day: {
        flex: flex[0],
        alignItems: 'center',
        flexDirection: 'row',
    },
    city_day_text: {
        justifyContent: 'center',
        alignSelf: 'auto',
        flex: 1,
        fontSize: 20,
        textAlign: 'center',
        color: '#95a5a6',
    },
    status: {
        flex: flex[1],
        fontSize: 20,
        textAlign: 'center',
        color: '#f1c40f',
        fontWeight: 'bold',
    },
    current: {
        textAlign: 'center',
        fontSize: 130,
        color: '#ffffff',
        flex: flex[2]
    },
    weather_icon: {
        fontFamily: 'WeatherIcons-Regular',
    },
})