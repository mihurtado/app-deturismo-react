import React, { Component } from 'react';
import Common from './Common';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';

export default class WeekView extends Component {
    componentDidMount() {

    }

  days() {
    if(this.props.data)
      return Common.week(this.props.data).map((item, index) => {
          var day = 
              <Text style={ styles.day }>
                  { item.week }{"\n"}
                  { item.date }
              </Text>
          var iconUp = 
              <Text style = {[styles.day_icon, styles.weather_icon]}>
                  { item.iconUp }
              </Text>
          var iconDown = 
              <Text style = {[styles.day_icon, styles.weather_icon]}>
                  { item.iconDown }
              </Text>
          var max = 
              <Text style={ styles.temp }>
                  Max{"\n"}
                  { item.max }°C
              </Text>
          var min = 
              <Text style={ styles.temp }>
                  Min{"\n"}
                  { item.min }°C
              </Text>
          return (
              <View style={ styles.dayCol } key={item.week}>
                {day}
                {iconUp}
                {min}
                {max}           
              </View>
          );
      });
    else
      return <View style={ styles.dayCol }/>
  }

  render() {
    if(this.props.data){
      return (
        <View style = { styles.week }>
          <View style = { styles.week_panel } >
              <View style={ styles.dayCol } />
              { this.days() }
              <View style={ styles.dayCol } />
          </View>
        </View>
      );
    }else{
      return(
        <View style = { styles.week } />
        );
    }
  }
}

var styles = StyleSheet.create({
    dayCol: {
        borderRightColor: '#4d667e',
        borderRightWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
    },
    weather_icon: {
        fontFamily: 'WeatherIcons-Regular',
    },
    week_panel: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    week: {
        alignItems: 'stretch',
        flex: 6,
    },
    day_icon: {
        fontSize: 20,
        color: '#ffffff',
        flex: 1,
    },
    day: {
        fontSize: 12,
        color: '#ffffff',
        flex: 1,
        textAlign: 'center',
    },
    temp: {
        fontSize: 12,
        color: '#ffffff',
        flex: 1,
        textAlign: 'center',
    }
})
