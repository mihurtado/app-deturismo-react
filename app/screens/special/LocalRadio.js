import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  WebView,
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export default class LocalRadio extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style = { styles.container }>
        <WebView
          source={{uri: 'https://tunein.com/radio/Mirasol-Fm-1075-s266295/'}}
        />
      </View>
    )
    
  }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
})