import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Modal,
  StatusBar
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
  Icon,
} from 'react-native-elements';
import { 
  GetItineraryIds,
  GetFacebookAccessToken,
  GetUserInformation,
} from '../../helpers/StoredData';
import { 
  DBConnect,
  GetListByIds,
} from '../../services/Objects';
import { 
  LOCAL_ASSETS_BASE, 
  URL_GET_MESSAGES,
  URL_SEND_MESSAGE,
} from '../../config/urls';
import { 
  GiftedChat,
  Send,
} from 'react-native-gifted-chat'


export default class Chat extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      lastPage: 0,
      senderInfo: null,
      receiverInfo: null,
      isLoading: true,
    }
  }

  getMessages(init = false) {
    fetch(URL_GET_MESSAGES, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        sender: this.state.senderInfo.id,
        receiver: this.state.receiverInfo.id,
        page: init? this.state.lastPage : this.state.lastPage + 1,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      const new_messages = responseJson.data.map((message) => {

        let sender = this.state.senderInfo
        if (message.sender != this.state.senderInfo.id) {
          sender = this.state.receiverInfo
        }

        return {
          _id: message.id,
          text: message.message,
          createdAt: message.created_at,
          user: {
            _id: sender.id,
            name: sender.name,
            avatar: 'https://graph.facebook.com/v2.5/' + sender.id + '/picture',
          },
        }
      })
      this.setState({
        messages: this.state.messages.concat(new_messages)
      })
    })
    .catch((error) => {
      console.log(error)
    })
    if (!init) {
      this.setState({
        lastPage: this.state.lastPage + 1,
      })
    } else {
      this.setState({
        isLoading: false,
      })
    }
  }

  componentWillMount() {
    this.setState({
      senderInfo: this.props.navigation.state.params.sender,
      receiverInfo: this.props.navigation.state.params.receiver,
    }, () => {
      this.getMessages(true)
    })
  }

  loadEarlier() {
    this.getMessages()
  }

  onSend(message = []) {
    fetch(URL_SEND_MESSAGE, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        sender: this.state.senderInfo.id,
        senderName: this.state.senderInfo.name,
        receiver: this.state.receiverInfo.id,
        message: message[0].text,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, message),
      }))      
    })
    .catch((error) => {
      console.log(error)
    })    
  }

  renderSend(props) {
    return (
      <Send {...props}>
        <View style={{marginRight: 10, marginBottom: 5}}>
          <Icon name='ios-send' type='ionicon' color='#3F51B1' />
        </View>
      </Send>
    );
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.topBar}>
          <View style={[styles.topBarPart, styles.topBarBack]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon name='ios-arrow-back' type='ionicon' color='#000' />
            </TouchableOpacity>
          </View>
          <View style={[styles.topBarPart, styles.topBarName]}>
            <Text style={styles.topBarNameText}>{this.state.receiverInfo.name}</Text>
          </View>
        </View>
        {this.state.isLoading ? (
          <View style={{flex: 1, paddingTop: 50}}>
            <ActivityIndicator />
          </View>
        ) : (
          <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            loadEarlier={true}
            onLoadEarlier={() => this.loadEarlier()}
            showUserAvatar={true}
            renderSend={this.renderSend}
            placeholder='Ingresa un mensaje...'
            user={{
              _id: this.state.senderInfo.id,
              name: this.state.senderInfo.name,
              avatar: 'https://graph.facebook.com/v2.5/' + this.state.senderInfo.id + '/picture',
            }}
          />
        )}
      </View>
    )
  }
}

const styles = {
  mainView: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  topBar: {
    backgroundColor: '#DDD',
    height: 50,
    flexDirection: 'row',
  },
  topBarPart: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  topBarBack: {
    flex: 1,
    paddingLeft: 15,
  },
  topBarName: {
    flex: 3,
  },
  topBarNameText: {
    fontSize: 18,
  },
}