import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken
} = FBSDK;

export default class FacebookLogin extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
  }

  facebookLogin() {
    
  }

  render() {

    return (
      <View>
        <Text style={styles.loginText}>Ingresa con tu Facebook para gestionar tus favoritos, itinerario y valorar publicaciones!</Text>
        <View style={styles.loginButtonContainer}>
          <LoginButton
            readPermissions={["email"]}
            onLoginFinished={
              (error, result) => {
                if (error) {
                  Alert.alert(
                    'Ha ocurrido un error',
                    result.error,
                    [
                      {text: 'Ok'},
                    ],
                    { cancelable: false }
                  )
                } else if (result.isCancelled) {
                  Alert.alert(
                    'Ha ocurrido un error',
                    'Has cancelado el inicio de sesión. Inténtalo nuevamente',
                    [
                      {text: 'Ok'},
                    ],
                    { cancelable: false }
                  )
                } else {
                  this.props.screenProps.facebookLogin()
                }
              }
            }
            onLogoutFinished={() => {
              this.props.screenProps.facebookLogout()
            }}/>
          </View>
      </View>
    )
  }

}

var styles = StyleSheet.create({
    loginText: {
      textAlign: 'center',
    },
    loginButtonContainer: {
      marginVertical: 20,
      flexDirection: 'row',
      justifyContent: 'center',
    },
})