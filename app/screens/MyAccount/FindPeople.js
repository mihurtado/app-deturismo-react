import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Modal,
} from 'react-native';
import { 
  List, 
  ListItem, 
  Button,
  Avatar,
  Icon,
} from 'react-native-elements';
import { 
  GetItineraryIds,
  GetFacebookAccessToken,
  GetUserInformation,
} from '../../helpers/StoredData';
import MapView from 'react-native-maps';
import { 
  DBConnect,
  GetListByIds,
} from '../../services/Objects';
import { 
  LOCAL_ASSETS_BASE, 
  URL_MAP_ICONS,
  URL_UPDATE_USER_LOCATION,
  URL_NEAR_USERS,
  URL_OPENED_CHATS,
} from '../../config/urls';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class FindPeople extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      nearPeople: [],
      openedChats: [],
      region: null,
      regionSet: false,
    }
  }

  updateNearUsers() {
    fetch(URL_NEAR_USERS, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: this.props.screenProps.userInfo.id,
        lat: this.props.screenProps.currentPosition.coords.latitude,
        lng: this.props.screenProps.currentPosition.coords.longitude,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        nearPeople: responseJson.data,
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  getOpenedChats() {
    console.log(this.props.screenProps.userInfo)
    fetch(URL_OPENED_CHATS, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: this.props.screenProps.userInfo.id,
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        openedChats: responseJson.data,
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  loadData() {
    if (this.props.screenProps.userInfo) {
      this.getOpenedChats()
      this.updateNearUsers()
      let intervalId = setInterval(this.updateNearUsers.bind(this), 10000);
      // store intervalId in the state so it can be accessed later:
      this.setState({intervalId: intervalId});
    }
    // Center to user's location
    const initialRegion = {
      latitude: this.props.screenProps.currentPosition.coords.latitude,
      longitude: this.props.screenProps.currentPosition.coords.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    };
    this.setState({
      region: initialRegion,
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.screenProps.userInfo != this.props.screenProps.userInfo) {
      clearInterval(this.state.intervalId)
      this.loadData()
    }

    if (this.props.navigation.state.params && this.props.navigation.state.params.reload) {
      if (this.props.screenProps.userInfo) {
        this.getOpenedChats()
      }
    }
  }

  componentDidMount() {
    this.loadData()
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    clearInterval(this.state.intervalId)
  }
 
  markerBublePressed(person, index) {
    // Erase not readed
    const openedChats = this.state.openedChats;
    openedChats[index].total_not_readed = 0;
    this.setState({
      openedChats: openedChats,
    })
    this.props.navigation.navigate('Chat', { ...{sender: this.props.screenProps.userInfo, receiver: person} });
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        {this.props.screenProps.userInfo == null ? (
          <View style={{flex: 1, padding: 20}}>
            <Text style={styles.loginText}>Debes iniciar sesión en Facebook para utilizar esta sección.</Text>        
          </View>
        ) : (
          <View style={styles.mainView}>
            <View style={styles.topBarContainer}>
              <ScrollView horizontal={true} contentContainerStyle={styles.topBarContentContainer}>
                {this.state.openedChats.length > 0 ? (this.state.openedChats.map((user, index) => (
                  <View key={user.id} style={{flexDirection: 'row'}}>
                    <Avatar
                      containerStyle={{borderRadius: 999, borderWidth: 2, borderColor: '#d6d7da',}}
                      medium
                      rounded
                      source={{uri: 'https://graph.facebook.com/v2.5/' + user.id + '/picture'}}
                      onPress={() => this.markerBublePressed(user, index)}
                      activeOpacity={0.7}
                    />
                    {user.total_not_readed > 0 && (
                      <Icon name="new" color="#FF0000" type="entypo" size={24} onPress={() => this.markerBublePressed(user, index)} style={styles.newMessageIcon} />
                    )}
                  </View>
                ))) : (
                  <Text style={styles.emptyChats}>No has iniciado conversaciones aún</Text>
                )}
              </ScrollView>
            </View>
            <View style={styles.container}>
              <MapView
                ref={ref => { this.map = ref; }}
                style={styles.map}
                scrollEnabled={true}
                zoomEnabled={true}
                pitchEnabled={true}
                rotateEnabled={true}
                showsUserLocation={true}
                loadingEnabled={true}
                region={this.state.region}
                onMapReady={() => {
                  this.setState({ regionSet: true });
                }}
                onRegionChange={(region) => {
                  if (!this.state.regionSet) return;
                  this.setState({
                    region
                  });
                }}
                toolbarEnabled={true}
              >
                {this.state.nearPeople.map((person, index) => (
                  <MapView.Marker
                    key={person.id}
                    coordinate={{latitude: Number(person.lat), longitude: Number(person.lng)}}
                    image={{uri: 'https://graph.facebook.com/v2.5/' + person.id + '/picture'}}
                    onCalloutPress={() => this.markerBublePressed(person, index)}
                  >
                    <MapView.Callout>
                      <View>
                        <Text>{person.name}</Text>
                        {/*marker.description != "" && (
                        <HTMLView addLineBreaks={false} value={'<div>' + marker.description.substring(0,40) + '...</div>'} />                  
                        )*/}
                      </View>
                    </MapView.Callout>
                  </MapView.Marker>
                ))}
              </MapView>
            </View>
          </View>
        )}
      </View>
      )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  mainView: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  topBarContainer: {
    backgroundColor: '#FFF',
    height: 60,
  },
  topBarContentContainer: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  emptyChats: {
    fontStyle: 'italic',
    fontSize: 12,
    marginTop: 18,
  },
  newMessageIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 100,
    marginTop: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    marginHorizontal: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
  },
  map_icon_list: {
    width: 20,
    height: 27,
  },
  titleStyleList: {
    paddingLeft: 15,
  },
  topButtonContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  actionButton: {
    marginBottom: 15,
    backgroundColor: '#4896EC', 
    borderRadius: 10, 
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: .8,
    shadowRadius: 2,
    paddingVertical: 7,
    paddingHorizontal: 10,
  },
});
