import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');
import { 
  DBConnect,
  GetListByIds,
} from '../../services/Objects';
import { LOCAL_ASSETS_BASE, URL_MAP_ICONS } from '../../config/urls';


export default class Favorites extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      objects: [],
    }
  }

  loadObjects() {
    DBConnect().then((db) => {
      GetListByIds(db, this.props.screenProps.favorites).then((objects) => {
        this.setState({
          isLoading: false,
          objects: objects,
        })
      })
    })
  }

  componentDidMount() {
    this.loadObjects()
  }

  componentWillReceiveProps(nextProps) {
    this.loadObjects()
  }

  onObjectPress = (object) => {
    this.props.navigation.navigate('PublicationDetailOnFavorites', { ...object });
  }

  render() {

    return (
      <View style = { styles.container }>
        {this.props.screenProps.userInfo == null ? (
          <View style={{flex: 1, padding: 20}}>
            <Text style={styles.loginText}>Debes iniciar sesión en Facebook para utilizar esta sección.</Text>        
          </View>
        ) : (
          <View>
            {this.state.isLoading || this.state.objects.length == 0 ? (
              <View style={{flex: 1, padding: 20}}>
                <Text style={styles.loginText}>Aún no completas tus favoritos.</Text>        
              </View>
              ) : (  
              <ScrollView>
                <List containerStyle={{marginTop: 5, marginBottom: 10}}>
                  {this.state.objects.map(object => {
                    return (
                    <ListItem
                      containerStyle={{paddingTop: 5, paddingBottom: 5,}}
                      key={object.id}
                      title={object.name}
                      leftIcon={<Image style={styles.map_icon_list} source={{uri: LOCAL_ASSETS_BASE + URL_MAP_ICONS + object.map_icon}} />}
                      titleStyle={styles.titleStyleList}
                      onPress={() => this.onObjectPress(object)}
                    />
                    )})}
                </List>
              </ScrollView>
            )}
          </View>
        )}
      </View>
    )
  }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map_icon_list: {
      width: 20,
      height: 27,
    },
    titleStyleList: {
      paddingLeft: 15,
    },
})