import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');
import { GetFacebookAccessToken } from '../../helpers/StoredData';
import FacebookLogin from './FacebookLogin';

export default class MyAccount extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      logedIn: false,
    }
  }
  

  componentDidMount() {
    GetFacebookAccessToken().then((token) => {
      if (token !== null) {
        this.setState({
          logedIn: true,
        })
      }
    })
  }

  render() {

    return (
      <View style = { styles.container }>
        <FacebookLogin {...this.props} />
      </View>
    )
  }

}

var styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 20,
    },
    loginText: {
      textAlign: 'center',
    },
    loginButtonContainer: {
      marginVertical: 20,
      flexDirection: 'row',
      justifyContent: 'center',
    },
})