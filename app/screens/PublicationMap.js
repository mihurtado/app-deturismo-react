import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Platform,
  Linking,
} from 'react-native';
import MapView from 'react-native-maps';

const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width;
const SCREEN_HEIGHT = height;
const ASPECT_RATIO = width / height;
const LATITUDE = -33.3648;
const LONGITUDE = -71.6706;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


export default class PublicationMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      publication: {
        name: '',
        description: '',
      },
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      marker: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
      },
      regionSet: false,
    }
    
  }

  componentDidMount() {
    const publication = this.props.navigation.state.params
    
    this.setState({
                    publication: publication,
                    region: {
                      latitude: Number(publication.latitude),
                      longitude: Number(publication.longitude),
                      latitudeDelta: LATITUDE_DELTA,
                      longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker: {
                      latitude: publication.latitude,
                      longitude: publication.longitude,
                    },
                  })
  }

  onHowToGetPress() {
    Platform.select({
        ios: () => {
            Linking.openURL('http://maps.apple.com/maps?daddr=' + this.state.region.latitude + ',' + this.state.region.longitude);
        },
        android: () => {
            Linking.openURL('http://maps.google.com/maps?daddr=' + this.state.region.latitude + ',' + this.state.region.longitude);
        }
    })();
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.back}
          onPress={() => this.props.navigation.goBack()}
        >
          <Text style={{ fontWeight: 'bold', fontSize: 20 }}>&larr;</Text>
        </TouchableOpacity>
        <MapView
          ref={ref => { this.map = ref; }}
          style={styles.map}
          scrollEnabled={true}
          zoomEnabled={true}
          pitchEnabled={true}
          rotateEnabled={true}
          showsUserLocation={true}
          loadingEnabled={true}
          region={this.state.region}
          toolbarEnabled={true}
          onMapReady={() => {
            this.setState({ regionSet: true });
          }}
          onRegionChange={(region) => {
            if (!this.state.regionSet) return;
            this.setState({
              region
            });
          }}
          >
          <MapView.Marker coordinate={{latitude: this.state.marker.latitude, longitude: this.state.marker.longitude}}>
            <MapView.Callout>
              <View>
                <Text>{this.state.publication.name}</Text>
              </View>
            </MapView.Callout>
          </MapView.Marker>
        </MapView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.onHowToGetPress()}
            style={[styles.bubble, styles.button]}
          >
            <Text>Cómo Llegar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  back: {
    position: 'absolute',
    top: 20,
    left: 6,
    backgroundColor: 'rgba(255,255,255,0.4)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 150,
    marginTop: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    marginHorizontal: 5,
  },
});
