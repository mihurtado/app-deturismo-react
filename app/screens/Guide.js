import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
  Platform,
  Share,
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
import BackgroundImage from '../helpers/BackgroundImage';
import { GetCategories, DBConnect } from '../services/Objects';
const { width, height } = Dimensions.get('window');
import { 
  LOCAL_ASSETS_BASE, 
  URL_CATEGORY_GUIDE_ICONS,
  SOCIAL_FACEBOOK_URL,
  SOCIAL_INSTAGRAM_URL,
  SOCIAL_YOUTUBE_URL,
  URL_WEBPAGE,
 } from '../config/urls';
import OneSignal from 'react-native-onesignal'; // Import package from node modules 

export default class Guide extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      categories: [],
    }
  }

  componentDidMount() {
    DBConnect().then((db) => {
      GetCategories(db).then((categories) => {
        this.setState({
          isLoading: false,
          categories: categories,
        })
      })
    })
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);

    // Check if url in data
    const data = openResult.notification.payload.additionalData
    if (data) {
      if (data.internal_url) {
        // Navigate
        if (data.internal_url == 'publication' && data.internal_url_id) {
          this.props.navigation.navigate('PublicationDetail', { ...{id: data.internal_url_id} })
        } else if (data.internal_url == 'events') {
          this.props.navigation.navigate('Events')
        } else if (data.internal_url == 'chat') {
          this.props.navigation.navigate('FindPeople', { ...{reload: true} })
        } else {
          try {
            if (data.internal_url && data.internal_url_id) {
              this.props.navigation.navigate(data.internal_url, { ...{id: data.internal_url_id} })
            } else {
              this.props.navigation.navigate(data.internal_url)
            }
          } catch (error) {
            console.warn(error)
          }
        }
      }
    }
  }

  onRegistered(notifData) {
    console.log("Device had been registered for push notifications!", notifData);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('registered', this.onRegistered);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('registered', this.onRegistered);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onCategoryPress = (category) => {
    if (category.total_subcategories > 1)
      this.props.navigation.navigate('SubCategories', { ...category });
    else
      this.props.navigation.navigate('CategoryList', { ...category });
  }

  shareAppPress() {
    let message = 'Te invitamos a instalar nuestra App Deturismo, disponible para IOS y Android desde el sitio Oficial.'
    if (Platform.OS === 'android') {
      message += ' http://www.deturismo.cl/'
    }
    Share.share({
      title: 'Deturismo',
      url: URL_WEBPAGE,
      message: message,
    }, {
      dialogTitle: 'Compartir App Deturismo',
    })
  }

  render() {
    return (
      <BackgroundImage source={require('../../assets/images/bg.jpg')}>
        <Image source={require('../../assets/images/logo.png')} style={styles.logoImage} resizeMode={'contain'}  />
        <ScrollView style={{flex: 1}}>

          {this.state.isLoading ? (
            <ActivityIndicator style={{marginTop: 50}} />
          ) : (
            <View>
              <View style={styles.buttonsContainer}>
                {this.state.categories.map((category, index) => (
                  <View key={category.id} style={[styles.eachButtonContainer]}>
                    <TouchableOpacity
                      onPress={() => this.onCategoryPress(category)}
                      >
                      <View>
                        {index % 2 == 0 ? (
                        <View>
                          <Image style={[styles.buttonIcon, styles.buttonIconLeft]} source={{uri: LOCAL_ASSETS_BASE + URL_CATEGORY_GUIDE_ICONS + category.menu_icon}} />
                          <View style={[styles.buttonTextContainer, styles.buttonTextContainerLeft, {backgroundColor: category.background_color || 'rgba(0,0,0,.6)'}]}>
                            <Text style={[styles.buttonText, styles.buttonTextLeft, {color: category.font_color || '#FFF' }]}>{category.name}</Text>
                          </View>
                        </View>
                        ) : (
                        <View>
                          <Image style={[styles.buttonIcon, styles.buttonIconRight]} source={{uri: LOCAL_ASSETS_BASE + URL_CATEGORY_GUIDE_ICONS + category.menu_icon}} />
                          <View style={[styles.buttonTextContainer, styles.buttonTextContainerRight, {backgroundColor: category.background_color || 'rgba(0,0,0,.6)'}]}>
                            <Text style={[styles.buttonText, styles.buttonTextRight, {color: category.font_color || '#FFF' }]}>{category.name}</Text>
                          </View>
                        </View>
                        )}
                        
                      </View>
                    </TouchableOpacity>
                  </View>
                ))}
              </View>
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() => this.shareAppPress()}>
                  <View>
                    <Image style={styles.shareImage} source={require('../../assets/images/logo_sq_small.png')} />
                    <View style={styles.buttonShare}>
                      <Text style={styles.buttonShareText}>Comparte</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => Linking.openURL(SOCIAL_FACEBOOK_URL)}>
                  <Image style={styles.socialImage} source={require('../../assets/images/social/facebook.png')} resizeMode={'contain'} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Linking.openURL(SOCIAL_INSTAGRAM_URL)}>
                  <Image style={[styles.socialImage, styles.socialImageIntagram]} source={require('../../assets/images/social/instagram.png')} resizeMode={'contain'} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Linking.openURL(SOCIAL_YOUTUBE_URL)}>
                  <Image style={styles.socialImage} source={require('../../assets/images/social/youtube.png')} resizeMode={'contain'} />
                </TouchableOpacity>
              </View>
            </View>
          )}
          
        </ScrollView>
      </BackgroundImage>
    )
  }

}

const styles = StyleSheet.create({
    logoImage: {
      marginTop: 10,
      marginBottom: 10,  
      width: width,
      height: width * 732 / 1800,
    },
    socialImage: {
      marginTop: 20,
      marginBottom: 10,
      marginHorizontal: 10,
      width: width / 3 - 20,
      height: width / 3 * 732 / 1800 - 20,
    },
    socialImageIntagram: {
      marginTop: 24,
    },
    list: {
      flex: 2,
    },
    buttonsContainer: {
      flex: 1,
      flexWrap: 'wrap',
      flexDirection: 'row',
      justifyContent: "space-around",
    },
    eachButtonContainer: {
      width: width / 2,
      marginBottom: 20,
    },
    button: {
      position: 'relative',
    },
    buttonIcon: {
      position: 'absolute',
      width: Platform.OS === 'ios' ? 55 : 50,
      height: Platform.OS === 'ios' ? 55 : 50,
      top: 0,
      zIndex: 10,
    },
    buttonIconLeft: {
      left: 7,
    },
    buttonIconRight: {
      right: 7,
    },
    buttonTextContainer: {
      paddingVertical: 10,
      marginTop: 8,
      borderColor: '#DDD',
      shadowColor: '#000',
      borderTopWidth: 0,
      shadowOffset: { width: 5, height: 5 },
      shadowOpacity: 1,
      shadowRadius: 3,
      borderWidth: 1,
      borderRadius: 5,
    },
    buttonTextContainerLeft: {
      marginLeft: 30,
      marginRight: 5,
    },
    buttonTextContainerRight: {
      marginRight: 30,
      marginLeft: 5,
    },
    buttonText: {
      lineHeight: 20,
      flex: 1,
      fontSize: 16,
      textAlign: 'center',
      fontFamily: 'RobotoCondensed-Regular',
    },
    buttonTextLeft: {
      paddingLeft: 27,
    },
    buttonTextRight: {
      paddingRight: 27,
    },
    buttonShare: {
      backgroundColor: 'rgba(0,0,0,.6)',
      paddingVertical: 5,
      marginTop: 8,
      shadowColor: '#000',
      shadowOffset: { width: 5, height: 5 },
      shadowOpacity: 1,
      shadowRadius: 3,
      borderWidth: 1,
      borderRadius: 5,
      marginLeft: 30,
      marginRight: 5,
    },
    buttonShareText: {
      color: '#FFF',
      lineHeight: 20,
      flex: 1,
      fontSize: 16,
      textAlign: 'center',
      fontFamily: 'RobotoCondensed-Regular',
      paddingLeft: 30,
      paddingRight: 10,
    },
    shareImage: {
      position: 'absolute',
      width: Platform.OS === 'ios' ? 50 : 42,
      height: Platform.OS === 'ios' ? 50 : 42,
      top: 0,
      zIndex: 10,
      left: 7,
    },
})
