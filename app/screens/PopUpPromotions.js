import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');
import { DBConnect, GetPopUpPromotion } from '../services/Objects';
import { LOCAL_ASSETS_BASE, URL_PROMOTIONS_IMAGES } from '../config/urls';

export default class PopUpPromotions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    }
  }

  loadPromotion() {
    DBConnect().then((db) => {
      GetPopUpPromotion(db).then((response) => {
        if (response === null) {
          this.props.handleClosePromotion()
        } else {
          this.setState({
            promotion: response,
            isLoading: false,
          })
        }
      })
    })
  }

  componentDidMount() {
    this.loadPromotion()
  }

  render() {
    if (this.state.isLoading && this.state.promotion !== null) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
        <View style={{flex: 1, backgroundColor: '#FFF'}}>
            <TouchableOpacity
            style={styles.back}
            onPress={() => this.props.handleClosePromotion()}
            >
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#FFF' }}>X</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL(this.state.promotion.url).catch(err => console.error('An error occurred', err))}>
                <Image source={{uri: LOCAL_ASSETS_BASE + URL_PROMOTIONS_IMAGES + this.state.promotion.image}} style={styles.image} resizeMode='contain' />
            </TouchableOpacity>
        </View>
    )
  }

}

const styles = {
    back: {
    position: 'absolute',
    top: 20,
    right: 6,
    backgroundColor: 'rgba(0,0,0,0.6)',
    padding: 8,
    borderRadius: 15,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },

  image: {
    width: width,
    height: height,
  },

}
