import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Modal,
  Image,
} from 'react-native';
import { 
  Button,
  Icon,
  List,
  ListItem,
} from 'react-native-elements';
import MapView from 'react-native-maps';
import { 
  DBConnect,
  GetMarkers,
  GetCategories,
} from '../services/Objects';
import { 
  LOCAL_ASSETS_BASE, 
  URL_MAP_ICONS,
} from '../config/urls';
import colors from '../helpers/colors';
import HTMLView from 'react-native-htmlview';

const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width;
const SCREEN_HEIGHT = height;
const ASPECT_RATIO = width / height;
const LATITUDE = -33.3648;
const LONGITUDE = -71.6706;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


export default class Mapa extends Component {
  constructor(props) {
    super(props);

    this.initialRegion = {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    };

    this.state = {
      markers: [],
      region: null,
      regionSet: false,
      modalFiltersVisible: false,
      categories: [],
      categoriesSelected: [],
    };
  }

  openFilter() {
    this.setState({
      modalFiltersVisible: true,
    })
  }

  doneFilter() {
    this.setState({
      modalFiltersVisible: false,
    })
  }

  loadCategories() {
    DBConnect().then((db) => {
      GetCategories(db).then((categories) => {
        this.setState({
          categories: categories,
          categoriesSelected: categories.map((category) => category.id),
        })
      })
    })
  }

  isCategorySelected = function(category) {
    if (this.state.categoriesSelected != null) {
      if (this.state.categoriesSelected.length > 0) {
        return this.state.categoriesSelected.indexOf(category.id) >= 0
      }
    }
    return false
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.categoriesSelected !== this.state.categoriesSelected) {
      this.setState({
        isLoading: true,
      })
      this.loadMarkers()
    }
  }

  loadMarkers() {
    DBConnect().then((db) => {
      GetMarkers(db, this.state.categoriesSelected).then((markers) => {
        this.setState({
          isLoading: false,
          markers: markers,
        })
      }).then(() => {
        this.reCenter()
      })
    })
  }

  componentDidMount() {
    this.loadCategories()
    this.setState({
      region: this.initialRegion,
    })
  }

  onCategoryPress = function(category) {
    const index = this.state.categoriesSelected.indexOf(category.id)
    if (index >= 0) {
      // remove
      this.setState({
        categoriesSelected: this.state.categoriesSelected.filter((_, i) => i !== index),
      })
    } else {
      // add
      this.setState({
        categoriesSelected: this.state.categoriesSelected.concat(category.id),
      })
    }
  } 

  /*
  // Now loading everything on load
  onRegionChangeComplete() {
    this.loadMarkers()
  }
  */

  reCenter() {
    this.setState({
      region: this.initialRegion,
    })
  }

  markerBublePressed(marker) {
    this.props.navigation.navigate('PublicationDetail', { ...{id: marker.id, name: marker.name} });
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref={ref => { this.map = ref; }}
          style={styles.map}
          scrollEnabled={true}
          zoomEnabled={true}
          pitchEnabled={true}
          rotateEnabled={true}
          showsUserLocation={true}
          loadingEnabled={true}
          region={this.state.region}
          onMapReady={() => {
            this.setState({ regionSet: true });
          }}
          onRegionChange={(region) => {
            if (!this.state.regionSet) return;
            this.setState({
              region
            });
          }}
          toolbarEnabled={true}
        >
          {this.state.markers.map(marker => (
            <MapView.Marker
              key={marker.id}
              coordinate={{latitude: Number(marker.latitude), longitude: Number(marker.longitude)}}
              image={{uri: LOCAL_ASSETS_BASE + URL_MAP_ICONS + marker.map_icon}}
              onCalloutPress={() => this.markerBublePressed(marker)}
            >
              <MapView.Callout>
                <View>
                  <Text>{marker.name}</Text>
                  {/*marker.description != "" && (
                  <HTMLView addLineBreaks={false} value={'<div>' + marker.description.substring(0,40) + '...</div>'} />                  
                  )*/}
                </View>
              </MapView.Callout>
            </MapView.Marker>
          ))}
        </MapView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.openFilter()}
            style={[styles.bubble, styles.button]}
          >
            <Text>Filtros</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.reCenter()}
            style={[styles.bubble, styles.button]}
          >
            <Text>Recentrar</Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalFiltersVisible}
          onRequestClose={() => this.setState({modalFiltersVisible: false})}
          >
          <View style={{marginTop: 25}}>
            <View style={styles.topButtonContainer}>
              <Button 
                buttonStyle={styles.actionButton}
                onPress={() => this.setState({categoriesSelected: this.state.categories.map((cat) => cat.id)})} 
                title='Todas'
                icon = {{name: 'check'}} />
              <Text style={styles.modalTitle}>Categorías</Text>
              <Button 
                small
                buttonStyle={styles.actionButton} 
                onPress={() => this.setState({categoriesSelected: []})} 
                icon = {{name: 'close', type: 'navigation'}}
                title='Ninguna' />
            </View>
            <ScrollView>
              <List containerStyle={{marginTop: 5, marginBottom: 10}}>
                {this.state.categories.map(category => {
                  let icon = {name: 'check', color: '#FFF'}
                  if (this.isCategorySelected(category))
                    icon = {name: 'check'}
                  if (category.id == "sobre-algarrobo") return null
                  return (
                  <ListItem
                    containerStyle={{paddingTop: 5, paddingBottom: 5,}}
                    key={category.id}
                    title={category.name}
                    leftIcon={<Image style={styles.map_icon_list} source={{uri: LOCAL_ASSETS_BASE + URL_MAP_ICONS + category.map_icon}} />}
                    rightIcon={icon}
                    titleStyle={styles.titleStyleList}
                    onPress={() => this.onCategoryPress(category)}
                  />
                  )})}
              </List>
              <Button 
                buttonStyle={styles.actionButton}
                onPress={() => this.doneFilter()} 
                title='Aceptar' />
            </ScrollView>
          </View>
        </Modal>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 100,
    marginTop: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    marginHorizontal: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
  },
  map_icon_list: {
    width: 20,
    height: 27,
  },
  titleStyleList: {
    paddingLeft: 15,
  },
  topButtonContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  actionButton: {
    marginBottom: 15,
    backgroundColor: '#4896EC', 
    borderRadius: 10, 
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: .8,
    shadowRadius: 2,
    paddingVertical: 7,
    paddingHorizontal: 10,
  },
});
