import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
const { width, height } = Dimensions.get('window');
import { LOCAL_ASSETS_BASE, URL_EVENTS_IMAGES } from '../config/urls';
import { DBConnect, GetEventsList } from '../services/Objects';
import HTMLView from 'react-native-htmlview';


export default class Information extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: "#FFFFFF",
    headerStyle: {
      backgroundColor:"#4896EC"
    },
  });

  constructor(props) {
    super(props)
    this.state = {
      events: [],
      start: 0,
      isLoading: true,
      isLoadingMore: false,
    }
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {

    this.setState({
      isLoading: true,
    })

    DBConnect().then((db) => {

      const resetStart = 0
      
      GetEventsList(db, resetStart, this.props.screenProps.currentPosition).then((response) => {        
        this.setState({
          isLoading: false,
          events: response,
          start: resetStart,
        })
      })
    })
  }

  isCloseToBottom = function({layoutMeasurement, contentOffset, contentSize}) {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  }

  loadMoreData() {
    if (!this.state.isLoadingMore) {
      this.setState({
        isLoadingMore: true,
      })

      const newStart = this.state.start + 7

      DBConnect().then((db) => {

        GetEventsList(db, newStart, this.props.screenProps.currentPosition).then((response) => {        
          this.setState({
            isLoadingMore: false,
            events: this.state.events.concat(response),
            start: newStart,
          })
        })
        
      })
    }
  }

  render() {

    return (
      <View style={styles.mainView}>

        {this.state.isLoading ? (
        <View style={{flex: 1, paddingTop: 50}}>
          <ActivityIndicator />
        </View>
        ) : (

        <ScrollView
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.loadMoreData();
            }
          }}
          scrollEventThrottle={400}
          >
          {this.state.events.length == 0 && (
            <Text style={{textAlign: 'center', marginTop: 50,}}>No hay eventos próximos disponibles</Text>
          ) }
          {this.state.events.map((object) => (
          <View style={styles.objectContainer} key={object.id}>
            <View style={styles.dateAndImageContainer}>
              <View style={styles.dateContainer}>
                <Text style={styles.dateDay}>{object.startDate.day}</Text>
                <Text style={styles.dateMonth}>{object.startDate.month}</Text>
                <Text style={styles.dateYear}>{object.startDate.year}</Text>
              </View>
              <View style={styles.imageContainer}>
                <Image style={styles.image} source={{uri: LOCAL_ASSETS_BASE + URL_EVENTS_IMAGES + object.image_url}} resizeMode="cover" />              
              </View>
            </View>
            <View style={styles.description}>
              <Text style={styles.title}>{object.name}</Text>
              <HTMLView addLineBreaks={true} stylesheet={descriptionStyles} value={'<div>' + object.description + '</div>'} />              
              {this.state.events.address && (
              <View style={{flexDirection:'row'}}>
                <Text style={styles.distance}>Ubicado en {object.address} | {object.distance} km</Text>
              </View>
              )}
            </View>
          </View>
          ))}

          {this.state.isLoadingMore && <ActivityIndicator style={{marginBottom: 10}} />}
        </ScrollView>
        )}
      </View>
    )
  }

}

const descriptionStyles = {
  div: {
    textAlign: 'justify',
    fontWeight: '200',
    color: '#444',
    fontSize: 15,
    marginBottom: 10,
    marginTop: 10,
  },
}

var styles = StyleSheet.create({

    mainView: {
      backgroundColor: '#FFF',
      flex: 1,
    },
  
    objectContainer: {
      margin: 0,
      marginBottom: 15,
    },

    dateAndImageContainer: {
      flexDirection: 'row',
      flex: 1,
    },

    dateContainer: {
      flex: 1,
      backgroundColor: '#4896EC',
      justifyContent: 'center',
      flexDirection: 'column',
    },

    dateDay: {
      color: '#FFF',
      fontSize: 60,
      textAlign: 'center',
    },

    dateMonth: {
      color: '#FFF',
      fontSize: 20,
      textAlign: 'center',
    },
    
    dateYear: {
      color: '#FFF',
      fontSize: 26,
      textAlign: 'center',
    },
    
    imageContainer: {
      flex: 2,
    },
  
    image: {
      width: width * 2 / 3,
      height: 250,
    },
  
    description: {
      marginTop: 5,
      marginLeft: 10,
      marginRight: 10,
      marginBottom: 15,
    },
  
    distance: {
      color: '#666',
    },
  
    title: {
      marginBottom: 5,
      color: '#4896EC',
      fontSize: 22,
      fontWeight: 'bold',
    },
  
})