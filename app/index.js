import React, { Component } from 'react';
import { 
  AppState, 
  View, 
  Modal,
  Alert,
  Text,
} from 'react-native';
import { Root, Tabs } from './config/router';
import PopUpPromotions from './screens/PopUpPromotions';
import Updater from './screens/Updater';
import { 
  GetFacebookAccessToken,
  GetFavoriteIds,
  GetItineraryIds,
  SetPublicationFavourite, 
  SetPublicationItinerary, 
  SetPublicationVisited,
  SetFacebookAccessToken,
  RemoveFacebookAccessToken,
  GetUserInformation,
  SetUserInformation,
  RemoveUserInformation,
} from './helpers/StoredData';
import Permissions from 'react-native-permissions'
const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken
} = FBSDK;
import { 
  URL_INIT_USER,
} from './config/urls';
import OneSignal from 'react-native-onesignal';

Text.defaultProps.allowFontScaling=false

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      appState: AppState.currentState,
      showingPromotion: false,
      checkingUpdate: true,
      currentPosition: null,
      itineraries: [],
      favorites: [],
      userInfo: null,
    }
    this.watchID = null
    this.handleClosePromotion = this.handleClosePromotion.bind(this);
    this.handleEndUpdate = this.handleEndUpdate.bind(this);
    this.updateItinerary = this.updateItinerary.bind(this);
    this.updateFavorite = this.updateFavorite.bind(this)
    this.facebookLogin = this.facebookLogin.bind(this)
    this.facebookLogout = this.facebookLogout.bind(this)
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);

    Permissions.check('location').then(response => {
      console.log(response)
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      this.setState({ locationPermission: response }, () => {
        console.log(response)
        if (response == 'undetermined') {
          Permissions.request('location').then(response => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            this.setState({ locationPermission: response })
          })
        }
        if (response == 'authorized') {
          navigator.geolocation.getCurrentPosition(
            (initialPosition) => {
              this.setState({currentPosition: initialPosition});
            },
            (error) => {
              console.warn(error.message)
              Alert.alert(
                'Error al obtener ubicación',
                'No hemos podido obtener tu ubicación. Por favor activa tu GPS para obtener una mejor experiencia.',
                [
                  {text: 'Ok'},
                ],
                { cancelable: false }
              )
            },
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
          );
          this.watchID = navigator.geolocation.watchPosition((lastPosition) => {
            this.setState({currentPosition: lastPosition});
          });
        }
      })
    })

    GetUserInformation().then((userInfo) => {
      this.setState({
        userInfo: userInfo,
      })
    })
    
    GetFavoriteIds().then((ids) => {
      this.setState({
        favorites: ids,
      })
    })
    GetItineraryIds().then((ids) => {
      this.setState({
        itineraries: ids,
      })
    })
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID)
    AppState.removeEventListener('change', this._handleAppStateChange)
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      // Load new promotion
      this.setState({
        // Disabled
        //showingPromotion: true,
      })
    }
    this.setState({appState: nextAppState});
  }

  handleClosePromotion() {
    this.setState({showingPromotion: false})
  }

  handleEndUpdate() {
    this.setState({
      checkingUpdate: false,
      // Disabled
      // showingPromotion: true,
    })
  }

  updateItinerary(objectId, newItinerary) {
    let promise = new Promise((resolve, reject) => {
      this.checkFacebookLogin().then((isLoged) => {
        if (isLoged) {
          SetPublicationItinerary(objectId, newItinerary).then(() => {
            const itineraries_changed = this.state.itineraries
            if (!newItinerary) {
              itineraries_changed.splice(itineraries_changed.indexOf(objectId), 1);
            } else {
              itineraries_changed.push(objectId)
            }
            this.setState({
              itineraries: itineraries_changed,
            })
            resolve(true)
          })
        } else {
          resolve(false)
        }
      })
    })
    return promise
  }

  updateFavorite(objectId, newFavorite) {
    let promise = new Promise((resolve, reject) => {
      this.checkFacebookLogin().then((isLoged) => {
        if (isLoged) {
          SetPublicationFavourite(objectId, newFavorite).then(() => {
            const favorites_changed = this.state.favorites
            if (!newFavorite) {
              favorites_changed.splice(favorites_changed.indexOf(objectId), 1);
            } else {
              favorites_changed.push(objectId)
            }
            this.setState({
              favorites: favorites_changed,
            })
            resolve(true)
          })
        } else {
          resolve(false)
        }
      })
    })
    return promise
  }

  facebookLogin() {
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        SetFacebookAccessToken(data.accessToken.toString())

        // Get onesignal information
        OneSignal.getPermissionSubscriptionState((status) => { 
         
          // Fetch server
          fetch(URL_INIT_USER, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              facebookToken: data.accessToken.toString(),
              oneSignalId: status.userId,
              oneSignalPushToken: status.pushToken,
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.sent == true) {
              SetUserInformation(responseJson.data)
              this.setState({
                userInfo: responseJson.data,
              })
            } else {
              console.log(responseJson.errors)
            }
          })
        })
      }
    )
  }

  facebookLogout() {
    RemoveFacebookAccessToken()
    RemoveUserInformation()
    this.setState({
      userInfo: null,
    })
  }

  render() {
    if (this.state.checkingUpdate) {
      return <Updater handleEndUpdate={this.handleEndUpdate} />
    }
    return (
      <View style={{flex: 1}}>
        <Root screenProps={{
          currentPosition: this.state.currentPosition,
          favorites: this.state.favorites,
          itineraries: this.state.itineraries,
          updateItinerary: this.updateItinerary,
          updateFavorite: this.updateFavorite,
          facebookLogin: this.facebookLogin,
          facebookLogout: this.facebookLogout,
          userInfo: this.state.userInfo,
        }} />
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.showingPromotion}
          onRequestClose={() => this.setState({showingPromotion: false})}
          >
          <PopUpPromotions handleClosePromotion={this.handleClosePromotion} />
        </Modal>
      </View>
    )
  }
}

export default App;