import {
  NetInfo,
} from 'react-native';

export async function CheckConnection() {
  let isConnected = false
  try {
    isConnected = await fetch("https://www.google.com")
  }
  catch(err) {
    isConnected = false
  }
  return isConnected
}