import { AsyncStorage } from 'react-native';

const default_json = {itinerary: false, favourite: false, visited: false, rank: null}

async function GetValue(key) {
  const value = await AsyncStorage.getItem(key);
  if (value !== null) {
    const jsonVal = JSON.parse(value)
    return jsonVal
  }
  return null
}

async function SetValue(key, newValue) {
  await AsyncStorage.setItem(key, JSON.stringify(newValue))
}

async function RemoveKey(key) {
  await AsyncStorage.removeItem(key)
}


function GetPublicationKey(publicationId) {
  return 'publicationStoredData-' + publicationId
}

export async function GetPublicationStoredData(publicationId) {
  return await GetValue(GetPublicationKey(publicationId))
}

async function SetPublicationStoredData(publicationId, data) {
  await SetValue(GetPublicationKey(publicationId), data)
}

export async function GetFavoriteIds() {
  const data = await GetValue('favorite-publications')
  if (data !== null) {
    return data
  }
  return []
}

export async function GetItineraryIds() {
  const data = await GetValue('itinerary-publications')
  if (data !== null) {
    return data
  }
  return []
}

async function AddFavoritePublication(publicationId) {
  let arr = await GetValue('favorite-publications')
  if (arr === null) {
    arr = []
  }
  arr.push(publicationId)
  await SetValue('favorite-publications', arr)
}

async function RemoveFavoritePublication(publicationId) {
  let arr = await GetValue('favorite-publications')
  if (arr !== null) {
    const index = arr.indexOf(publicationId)
    if (index > -1) {
      arr.splice(index, 1)
    }
    await SetValue('favorite-publications', arr)
  }
}

async function AddItineraryPublication(publicationId) {
  let arr = await GetValue('itinerary-publications')
  if (arr === null) {
    arr = []
  }
  arr.push(publicationId)
  await SetValue('itinerary-publications', arr)
}

async function RemoveItineraryPublication(publicationId) {
  const arr = await GetValue('itinerary-publications')
  if (arr !== null) {
    const index = arr.indexOf(publicationId)
    if (index > -1) {
      arr.splice(index, 1)
    }
    await SetValue('itinerary-publications', arr)
  }
}

export async function GetCurrentDatabaseVersion() {
  const data = await GetValue('database-version')
  if (data !== null) {
    return data.version
  }
  return null
}


export async function UpdateCurrentDatabaseVersion(newVersion) {
  const newValue = {version: newVersion}
  await AsyncStorage.setItem('database-version', JSON.stringify(newValue))
  return null
}

export async function GetPublicationItinerary(publicationId) {
  const data = await GetPublicationStoredData(publicationId)
  if (data !== null) {
    return data.itinerary
  }
  return false
}

export async function GetPublicationFavourite(publicationId) {
  const data = await GetPublicationStoredData(publicationId)
  if (data !== null) {
    return data.favourite
  }
  return false
}

export async function GetPublicationVisited(publicationId) {
  const data = await GetPublicationStoredData(publicationId)
  if (data !== null) {
    return data.visited
  }
  return false
}

export async function SetPublicationFavourite(publicationId, newValue) {
  let data = await GetPublicationStoredData(publicationId)
  if (data === null) {
    data = JSON.parse(JSON.stringify(default_json))
  }
  data.favourite = newValue
  if (data.favourite) {
    AddFavoritePublication(publicationId)
  } else {
    RemoveFavoritePublication(publicationId)
  }
  await SetPublicationStoredData(publicationId, data)
}

export async function SetPublicationItinerary(publicationId, newValue) {
  let data = await GetPublicationStoredData(publicationId)
  if (data === null) {
    data = JSON.parse(JSON.stringify(default_json))
  }
  data.itinerary = newValue
  if (data.itinerary) {
    AddItineraryPublication(publicationId)
  } else {
    RemoveItineraryPublication(publicationId)
  }
  await SetPublicationStoredData(publicationId, data)
}

export async function SetPublicationVisited(publicationId, newValue) {
  let data = await GetPublicationStoredData(publicationId)
  if (data === null) {
    data = JSON.parse(JSON.stringify(default_json))
  }
  data.visited = newValue
  await SetPublicationStoredData(publicationId, data)
}

export async function GetFacebookAccessToken() {
  return await GetValue('facebook-access-token')
}

export async function SetFacebookAccessToken(data) {
  await SetValue('facebook-access-token', data)
}

export async function RemoveFacebookAccessToken() {
  await AsyncStorage.removeItem('facebook-access-token')
}

export async function RemoveUserInformation() {
  await AsyncStorage.removeItem('user-information')
}

export async function GetUserInformation() {
  return await GetValue('user-information')
}

export async function SetUserInformation(data) {
  await SetValue('user-information', data)
}

export async function GetDownloadsRemaining() {
  let final = {}
  let data = await GetValue('download-remaining')
  if (data !== null) {
    data = JSON.parse(data)

    for (let element in data) {
      const key = 'download-remaining-' + element
      const data_e = await GetValue(key)
      if (data_e === null) {
        final[element] = data[element]
      }
    }
  }
  return final
}

export async function GetDownloadsDownloaded() {
  let final = []
  let data = await GetValue('download-remaining')
  if (data !== null) {
    data = JSON.parse(data)

    for (let element in data) {
      const key = 'download-remaining-' + element
      const data_e = await GetValue(key)
      if (data_e == 1) {
        final.push(element)
      }
    }
  }
  return final
}

export async function RemoveDownloadRemaining(id) {
  const key = 'download-remaining-' + id
  await SetValue(key, 1)
}

export async function GetTotalDownloadsRemaining() {
  let total = 0
  let data = await GetValue('download-remaining')
  if (data !== null) {
    data = JSON.parse(data)

    for (let element in data) {
      const key = 'download-remaining-' + element
      const data_e = await GetValue(key)
      if (data_e === null) {
        total += 1
      }
    }
  }
  return total
}

export async function SetDownloadsRemaining(data) {
  const data_json = JSON.stringify(data)
  await SetValue('download-remaining', data_json)
}

export async function ResetDownloadsRemainig() {
  const keys = await AsyncStorage.getAllKeys()
  for (index in keys) {
    const key = keys[index]
    //console.log(key.substring(0, 18))
    if (key.substring(0, 18) == "download-remaining") {
      await AsyncStorage.removeItem(key)
    }
  }
  return null
}