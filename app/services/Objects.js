import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);
SQLite.enablePromise(true);

export async function DBConnect() {
  const db = await SQLite.openDatabase(
    {name : "database", readOnly: true, createFromLocation : "/database.sqlite"}, 
    () => {console.log('Database loaded correctly')}, 
    (err) => {console.log('Error while opening database: ' + err)}
  )
  return db
}

export async function DBClose(db) {
  return await db.close();
}

export async function GetCategories(db) {
  const response = await db.executeSql(`SELECT c1.*, count(*) as total_subcategories 
                                        FROM categories c1 LEFT JOIN categories c2 
                                        ON c1.id = c2.parent 
                                        WHERE c1.parent is null 
                                        GROUP BY c1.id
                                        ORDER BY c1.orden ASC`)
  return Array.from(response[0].rows.raw())
}

export async function GetSubCategories(db, categoryId) {
  const response = await db.executeSql('SELECT * FROM categories WHERE parent = (?) ORDER BY orden ASC', [categoryId])
  return Array.from(response[0].rows.raw())
}

export async function GetCategoryPromotions(db, categoryId) {
  const response = await db.executeSql(`SELECT * FROM promotions p, promotion_shows s
                                        WHERE p.id = s.promotion_id
                                        AND datetime(p.start) <= datetime('now') and (p.end is null or datetime(p.end) >= datetime('now'))                                        
                                        AND s.location_id = (?)`, [categoryId])
  if (response[0].rows.length > 0)
    return Array.from(response[0].rows.raw())
  return null
}

// Only one
export async function GetCategoryPromotion(db, categoryId) {
  const response = await db.executeSql(`SELECT * FROM promotions p, promotion_shows s
                                        WHERE p.id = s.promotion_id
                                        AND s.location_id = (?)
                                        AND datetime(p.start) <= datetime('now') and (p.end is null or datetime(p.end) >= datetime('now'))
                                        ORDER BY RANDOM() LIMIT 1`, [categoryId])
  const result = Array.from(response[0].rows.raw())
  const promotion = result[0]
  return promotion
}

export async function GetPopUpPromotion(db) {
  const response = await db.executeSql(`SELECT * FROM promotions p
                                        WHERE p.show_popup = 1
                                        AND datetime(p.start) <= datetime('now') and (p.end is null or datetime(p.end) >= datetime('now'))                                        
                                        ORDER BY RANDOM() LIMIT 1`, [])
  const result = Array.from(response[0].rows.raw())
  if (result.length > 0) {
    const promotion = result[0]
    return promotion
  }
  return null
}

async function GetObjectsListCore(db, categoryId, order, retrieved, location, search, onlyPromoted) {
  let order_fix = ''
  let order_dir_fix = ''

  switch (order) {

    case 'destacados':
      order_fix = 'orden'
      orden_dir_fix = 'ASC'
      break

    case 'valoracion':
      order_fix = 'votes_avg';
      orden_dir_fix = 'DESC';
      break;
    
    case 'distancia':
      if (location !== null) {
        order_fix = 'ifnull(distance, 9999999)';
        orden_dir_fix = 'ASC';
      }
      else
      {
        order_fix = 'random()';
        orden_dir_fix = '';
      }
      break;

    default:
      order_fix = 'random()';
      orden_dir_fix = '';
      break;
  }
  
  let query = `SELECT p.*, IFNULL(img.url, 'default.jpg') as main_image `
  const bindParams = []

  if (location !== null) {
    query += `, CASE
                  WHEN latitude is not null and longitude is not null then ROUND( ( ((?) - latitude)*185.32*((?) - latitude)*185.32 + ((?) - longitude)*92.86*((?) - longitude)*92.86 ), 2)
                  ELSE NULL
                END AS distance `
    bindParams.push(location.coords.latitude, location.coords.latitude, location.coords.longitude, location.coords.longitude)
  }

  query += `FROM publications p
            JOIN categories c ON p.category_id = c.id
            LEFT JOIN publication_images img ON (p.id = img.publication_id AND img.is_main = 1)
            WHERE p.active = 1 AND ( c.id = (?) or c.parent = (?)) `

  if (onlyPromoted === true) {
    query += ' AND p.orden is not null '
  } else if (onlyPromoted === false) {
    query += ' AND p.orden is null '
  }

  bindParams.push(categoryId, categoryId)

  if (search != '') {
    query += `AND ( p.name LIKE (?) OR p.description LIKE (?) OR p.address LIKE (?) ) `
    const likeWord = '%'+search+'%'
    bindParams.push(likeWord, likeWord, likeWord)
  }

  if (retrieved !== null) {

    let question_marks = ''
    for (const i = 0; i < retrieved.length; i++) {
      question_marks += '?,'
    } 
    question_marks = question_marks.slice(0, -1)
    query += `AND p.id NOT IN (${question_marks}) `
    retrieved.forEach((id) => {
      bindParams.push(id)
    })
  }

  query += `ORDER BY ${order_fix} ${orden_dir_fix} 
           LIMIT 7`

  const response = await db.executeSql(query, bindParams)
  
  return Array.from(response[0].rows.raw())
}

export async function GetObjectsList(db, categoryId, order, retrieved, location, search) {
  if (order == 'default') {
    const ordenados = await GetObjectsListCore(db, categoryId, 'destacados', retrieved, location, search, true)
    const rand = await GetObjectsListCore(db, categoryId, order, retrieved, location, search, false)
    return ordenados.concat(rand)
  }
  return await GetObjectsListCore(db, categoryId, order, retrieved, location, search, null)
}


export async function GetListByIds(db, ids) {
 
  const bindParams = []
  let query = `SELECT p.*, 
              CASE
                WHEN c.map_icon is not null THEN c.map_icon
                WHEN c.parent is not null THEN (SELECT c2.map_icon FROM categories c2 WHERE c2.id = c.parent) 
                ELSE NULL
              END AS map_icon
              FROM publications p, categories c 
              WHERE p.category_id = c.id AND
              p.active = 1 AND 
              p.id IN ( `

  for(let i = 0; i < ids.length; i++) {
    query += '?,'
    bindParams.push(ids[i])
  }
  if (ids.length > 0)
    query = query.slice(0, -1)
  
  query += ')'

  const response = await db.executeSql(query, bindParams)
  
  return Array.from(response[0].rows.raw())
}

export async function GetEventsList(db, start, location) {
  
  let query = `SELECT e.* `
  const bindParams = []

  if (location !== null) {
    query += `, CASE
                  WHEN latitude is not null and longitude is not null then ROUND( ( ((?) - latitude)*185.32*((?) - latitude)*185.32 + ((?) - longitude)*92.86*((?) - longitude)*92.86 ), 2)
                  ELSE NULL
                END AS distance `
    bindParams.push(location.coords.latitude, location.coords.latitude, location.coords.longitude, location.coords.longitude)
  }

  query += `FROM events e
            WHERE datetime(e.start) >= datetime('now') or (e.end is null or datetime(e.end) >= datetime('now')) 
            ORDER BY e.start ASC 
            LIMIT 7 OFFSET (?)`

  bindParams.push(start)

  const response = await db.executeSql(query, bindParams)
  const result = response[0].rows.raw()

  result.forEach(function(element, index, theArray) {
    const dateParsed = new Date(element.start.replace(" ", "T"));
    const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    theArray[index].startDate = {day: dateParsed.getDate(), month: months[dateParsed.getMonth()], year: dateParsed.getFullYear()}
  });
  
  return result
}

export async function GetObject(db, objectId, location) {
  let query = null
  let response = null
  let result = null
  if (location !== null) {
    query = `SELECT p.*, 
            CASE
              WHEN latitude is not null and longitude is not null then ROUND( ( ((?) - latitude)*185.32*((?) - latitude)*185.32 + ((?) - longitude)*92.86*((?) - longitude)*92.86 ), 2)
              ELSE NULL
            END AS distance,
            IFNULL(img.url, 'default.jpg') as main_image
            FROM publications p
            LEFT JOIN publication_images img ON (p.id = img.publication_id AND img.is_main = 1)
            WHERE p.id = (?)`
    
    response = await db.executeSql(query, [location.coords.latitude, location.coords.latitude, location.coords.longitude, location.coords.longitude, objectId])
  } else {
    query = `SELECT p.*, IFNULL(img.url, 'default.jpg') as main_image
             FROM publications p
             LEFT JOIN publication_images img ON (p.id = img.publication_id AND img.is_main = 1)
             WHERE p.id = (?)`
    response = await db.executeSql(query, [objectId])
  }
  result = Array.from(response[0].rows.raw())
  const publication = result[0]

  query = `SELECT img.* FROM publication_images img WHERE img.publication_id = (?)`
  response = await db.executeSql(query, [objectId])
  result = Array.from(response[0].rows.raw())

  publication.images = result

  return publication
}

export async function GetMarkers(db, categories) {
  let query = ` SELECT p.*, 
                CASE
                  WHEN c.map_icon is not null THEN c.map_icon
                  WHEN c.parent is not null THEN (SELECT c2.map_icon FROM categories c2 WHERE c2.id = c.parent) 
                  ELSE NULL
                END AS map_icon
                FROM publications p, categories c 
                WHERE p.category_id = c.id AND p.active = 1 `
  const bindParams = []

  if (categories != null) {
    let question_marks = ''
    for (const i = 0; i < categories.length; i++) {
      question_marks += '?,'
    } 
    question_marks = question_marks.slice(0, -1)
    query += `AND (c.id IN (${question_marks}) OR c.parent IN (${question_marks})) `
    for (const j = 0; j < 2; j++) {
      categories.forEach((categoryId) => {
        bindParams.push(categoryId)
      })
    }
    
  }
  
  response = await db.executeSql(query, bindParams)
  return Array.from(response[0].rows.raw())
}

export async function GetNaturalSounds(db) {
  
  let query = `SELECT * FROM natural_sounds`
  const bindParams = []

  const response = await db.executeSql(query, bindParams)
  const result = response[0].rows.raw()
  
  return result
}

import { URL_CATEGORY_GUIDE_ICONS, 
         URL_MAP_ICONS, 
         URL_PUBLICATION_IMAGES, 
         URL_PROMOTIONS_IMAGES, 
         URL_EVENTS_IMAGES, 
         URL_NATURAL_SOUNDS_IMAGES,
        } from '../config/urls';

export async function GetAllPicturesUrl(db) {
  let query = null
  let response = null
  let urls = {}
  let result = null

  let id = null
  
  query = ` SELECT menu_icon as url
            FROM categories
            WHERE menu_icon is not null `

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_CATEGORY_GUIDE_ICONS, name: element.url}
  })

  query = ` SELECT map_icon as url
            FROM categories
            WHERE map_icon is not null `

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_MAP_ICONS, name: element.url}
  })

  query = ` SELECT url as url
            FROM publication_images `

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_PUBLICATION_IMAGES, name: element.url}
  })

  // Add default image
  id = 'default'
  urls[id] = {id: id, folder: URL_PUBLICATION_IMAGES, name: 'default.jpg'}

  query = ` SELECT image as url
            FROM promotions 
            WHERE image is not null`

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_PROMOTIONS_IMAGES, name: element.url}
  })

  query = ` SELECT image_url as url
            FROM events 
            WHERE image_url is not null`

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_EVENTS_IMAGES, name: element.url}
  })

  query = ` SELECT picture as url
            FROM natural_sounds 
            WHERE picture is not null`

  response = await db.executeSql(query, [])
  result = Array.from(response[0].rows.raw())
  result.forEach((element) => {
    id = element.url.split('.')[0]
    urls[id] = {id: id, folder: URL_NATURAL_SOUNDS_IMAGES, name: element.url}
  })

  return urls
}
