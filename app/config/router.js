import React from 'react';
import {
  Platform,
} from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import Guide from '../screens/Guide';
import SubCategories from '../screens/SubCategories';
import CategoryList from '../screens/CategoryList';
import PublicationDetail from '../screens/PublicationDetail';
import PublicationMap from '../screens/PublicationMap';
import Mapa from '../screens/Mapa';
import Information from '../screens/Information/Information';
import AboutOurApp from '../screens/Information/AboutOurApp';
import PublishWithUs from '../screens/Information/PublishWithUs';
import MyAccount from '../screens/MyAccount/MyAccount';
import Favorites from '../screens/MyAccount/Favorites';
import Itinerary from '../screens/MyAccount/Itinerary';
import FindPeople from '../screens/MyAccount/FindPeople';
import Chat from '../screens/MyAccount/Chat';
import Events from '../screens/Events';
import HistoricInfo from '../screens/special/HistoricInfo';
import HowToGet from '../screens/special/HowToGet';
import Climate from '../screens/special/Climate';
import Weather from '../screens/special/Weather';
import LocalRadio from '../screens/special/LocalRadio';
import NaturalSounds from '../screens/special/NaturalSounds';

export const GuideStack = StackNavigator({
  GuideMain: {
    screen: Guide,
    navigationOptions: {
      title: 'Guía',
      headerStyle: {display: 'none'},
      headerTruncatedBackTitle: 'Volver',
    },
  },

  SubCategories: {
    screen: SubCategories,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.name}`,
      headerTruncatedBackTitle: 'Volver',
    }),
  },

  CategoryList: {
    screen: CategoryList,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.name}`,
      headerTruncatedBackTitle: 'Volver',
    }),
  },
  
  PublicationDetail: {
    screen: PublicationDetail,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.name}`,
      headerTruncatedBackTitle: 'Volver',
    }),
  },

  PublicationMap: {
    screen: PublicationMap,
    navigationOptions: ({ navigation }) => ({
      title: 'Map',
      headerStyle: {display: 'none'},
    }),
  },
  HistoricInfo: {
    screen: HistoricInfo,
    navigationOptions: {
      title: 'Reseña Histórica',
    },
  },
  HowToGet: {
    screen: HowToGet,
    navigationOptions: {
      title: 'Cómo Llegar',
    },
  },
  Climate: {
    screen: Climate,
    navigationOptions: {
      title: 'Clima',
    },
  },
  Weather: {
    screen: Weather,
    navigationOptions: {
      title: 'El Tiempo',
    },
  },
  LocalRadio: {
    screen: LocalRadio,
    navigationOptions: {
      title: 'Radio Local',
    },
  },
  NaturalSounds: {
    screen: NaturalSounds,
    navigationOptions: {
      title: 'Sonidos Naturales',
    },
  },
  
}, {headerMode: 'screen'});

export const InformationStack = StackNavigator({
  Information: {
    screen: Information,
    navigationOptions: {
      title: 'Informaciones',
      headerTruncatedBackTitle: 'Volver',
    },
  },
  AboutOurApp: {
    screen: AboutOurApp,
    navigationOptions: {
      title: 'Acerca de nuestra App',
    },
  },
  PublishWithUs: {
    screen: PublishWithUs,
    navigationOptions: {
      title: 'Publica con nosotros',
    },
  },
  
}, {
  headerMode: 'screen',
});


export const FavoritesStack = StackNavigator({
  Favorites: {
    screen: Favorites,
  },
  PublicationDetailOnFavorites: {
    screen: PublicationDetail,
  },
}, {headerMode: 'none'});

export const ItineraryStack = StackNavigator({
  Itinerary: {
    screen: Itinerary,
  },
  PublicationDetailOnItinerary: {
    screen: PublicationDetail,
  },
}, {headerMode: 'none'});

export const FindPeopleStack = StackNavigator({
  FindPeople: {
    screen: FindPeople,
  },
  Chat: {
    screen: Chat,
  },
}, {headerMode: 'none'});

export const MyAccountTab = TabNavigator({
  MyAccount: {
    screen: MyAccount,
    navigationOptions: {
      tabBarLabel: 'Mi Cuenta',
      tabBarIcon: ({ tintColor }) => <Icon name="user" type="simple-line-icon" size={20} color={tintColor} />,
    },
  },
  Favorites: {
    screen: FavoritesStack,
    navigationOptions: {
      tabBarLabel: 'Mis Favoritos',
      tabBarIcon: ({ tintColor }) => <Icon name="ios-information-circle" type="ionicon" size={20} color={tintColor} />,
    },
  },
  Itinerary: {
    screen: ItineraryStack,
    navigationOptions: {
      tabBarLabel: 'Mi Itinerario',
      tabBarIcon: ({ tintColor }) => <Icon name="ios-information-circle" type="ionicon" size={20} color={tintColor} />,
    },
  },
  FindPeople: {
    screen: FindPeopleStack,
    navigationOptions: {
      tabBarLabel: 'Buscar personas',
      tabBarIcon: ({ tintColor }) => <Icon name="ios-people" type="ionicon" size={20} color={tintColor} />,
    },
  },
}, {
  tabBarPosition: 'top',
  tabBarOptions: {
    activeTintColor: '#fff',
    labelStyle: {
      fontSize: 12,
    },
    style: {
      backgroundColor: '#000C1C',
      height: 45,
      marginTop: 20,
    },
    activeBackgroundColor: '#333F4F',
    showIcon: false,
    showLabel: true,
  }
  
});

export const Tabs = TabNavigator({
  Guide: {
    screen: GuideStack,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: 'Guía',
      tabBarIcon: ({ tintColor }) => <Icon name="open-book" type="entypo" size={20} color={tintColor} />,
      tabBarOnPress: (tab, jumpToIndex) => {
        if (navigation.state.key != "GuideMain")
          navigation.navigate('GuideMain')
      },
    }),
  },
  Events: {
    screen: Events,
    navigationOptions: {
      tabBarLabel: 'Eventos',
      tabBarIcon: ({ tintColor }) => <Icon name="calendar" type="font-awesome" size={20} color={tintColor} />,
    },
  },
  Mapa: {
    screen: Mapa,
    navigationOptions: {
      tabBarLabel: 'Mapa',
      tabBarIcon: ({ tintColor }) => <Icon name="map-marker" type="font-awesome" size={24} color={tintColor} />,
    },
  },
  MyAccount: {
    screen: MyAccountTab,
    navigationOptions: {
      tabBarLabel: 'Mi Cuenta',
      tabBarIcon: ({ tintColor }) => <Icon name="user" type="simple-line-icon" size={20} color={tintColor} />,
    },
  },
  Information: {
    screen: InformationStack,
    navigationOptions: {
      tabBarLabel: 'Info',
      tabBarIcon: ({ tintColor }) => <Icon name="ios-information-circle" type="ionicon" size={24} color={tintColor} />,
    },
  },
}, {
  tabBarPosition: 'bottom',
  tabBarOptions: {
    activeTintColor: '#FFFFFF',
    labelStyle: {
      fontSize: 12,
    },
    style: {
      backgroundColor: '#000C1C',
      height: 45,
    },
    activeBackgroundColor: '#333F4F',
    showLabel: false,
    showIcon: true,
  },  
  swipeEnabled: false,
  lazy: (Platform.OS === 'ios') ? false : true,
  animationEnabled: (Platform.OS === 'ios') ? true : false,
});

export const Root = StackNavigator({
  Tabs: {
    screen: Tabs,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});