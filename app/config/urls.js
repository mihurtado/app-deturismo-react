import RNFS from 'react-native-fs';

export const URL_WEBPAGE = 'https://deturismo.cl/algarrobo'
export const URL_PUBLISH_FORM = 'https://deturismo.cl/algarrobo/publish'
export const URL_SEND_COMMENT = 'https://deturismo.cl/algarrobo/vote'
export const URL_VIEW_COMMENTS = 'https://deturismo.cl/algarrobo/comments'
export const URL_PUBLICATION_CONTACT_FORM = 'https://deturismo.cl/algarrobo/publication_contact'
export const URL_INIT_USER = 'https://deturismo.cl/algarrobo/init_user'
export const URL_UPDATE_USER_LOCATION = 'https://deturismo.cl/algarrobo/update_user_location'
export const URL_NEAR_USERS = 'https://deturismo.cl/algarrobo/near_users'
export const URL_GET_MESSAGES = 'https://deturismo.cl/algarrobo/get_messages'
export const URL_SEND_MESSAGE = 'https://deturismo.cl/algarrobo/send_message'
export const URL_OPENED_CHATS = 'https://deturismo.cl/algarrobo/opened_chats'
export const URL_CHAT_FRAME = 'https://deturismo.cl/algarrobo/chat'
export const URL_ASSETS_BASE = 'https://deturismo.cl/app_assets'
export const URL_DATABASE_VERSION = '/database/version.info?v=' + new Date().getTime()
export const URL_DATABASE = '/database/database.sqlite'
export const URL_MAP_ICONS = '/images/map_icons/'
export const URL_CATEGORY_GUIDE_ICONS = '/images/guide_icons/'
export const URL_PUBLICATION_IMAGES = '/images/publications/'
export const URL_PROMOTIONS_IMAGES = '/images/promotions/'
export const URL_EVENTS_IMAGES = '/images/events/'
export const URL_NATURAL_SOUNDS_IMAGES = '/images/natural_sounds/'
export const URL_NATURAL_SOUNDS_SOUNDS = '/sounds/natural_sounds/'

// SOCIAL
export const SOCIAL_FACEBOOK_URL = 'https://deturismo.cl/algarrobo/social/facebook'
export const SOCIAL_INSTAGRAM_URL = 'https://deturismo.cl/algarrobo/social/instagram'
export const SOCIAL_YOUTUBE_URL = 'https://deturismo.cl/algarrobo/social/youtube'

export const LOCAL_DOCUMENTS_PATH = RNFS.DocumentDirectoryPath
export const LOCAL_ASSETS_PATH = LOCAL_DOCUMENTS_PATH + '/assets'
export const LOCAL_ASSETS_BASE = 'file://' + LOCAL_ASSETS_PATH
